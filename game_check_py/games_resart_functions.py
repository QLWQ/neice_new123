#/usr/bin/env python

#by Jack

import get_process
import time
import traceback
from run_linux_command import run_command

today_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))

def restartting(service_name,write_logs,game_name,file_dir,file_name):
    if get_process.get_process(service_name.strip(),write_logs) ==1:
        write_logs.warning('{} is exist,restart cancel ...'.format(service_name.strip()))
        exit()

    else:
        try:
            if service_name =='mj_center':
                run_command('cd /app/game/joys/mj/bin/ && rm offline_pay_roomcard.ing',write_logs)

            process_name_cmd = run_command('cat {}{}|grep \'./\'|grep -w {}'.format(file_dir,file_name,service_name.strip()))

            if process_name_cmd[1].startswith(b'nohup') or process_name_cmd[1].endswith(b'&'):
                run_command('cd {} && {}'.format(file_dir, process_name_cmd[1]),write_logs,stdout=True)

            else:
                start_err_log ='/tmp/mnt_start_err_{}.log'.format(today_date)
                run_command(r'echo `date "+%Y-%m-%d %H:%M:%S"`>>{2} && cd {0} && nohup {1} >/dev/null 2>>{2} &'
                            .format(file_dir, process_name_cmd[1].strip(),start_err_log),write_logs,stdout=True)

            time.sleep(1)

            if get_process.get_process(service_name.strip(),write_logs) ==1:
                write_logs.warning('{} is startting success...'.format(service_name.strip()))

            else:
                write_logs.warning('{} is startting fail...'.format(service_name.strip()))

        except:
            print(traceback.print_exc())
            write_logs.error('{} is startting error...'.format(service_name.strip()))


 # startting_mj ('mj_hall')