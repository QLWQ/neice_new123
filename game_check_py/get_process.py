#/usr/bin/env python

#by Jack

from run_linux_command import run_command

def get_process(name,write_logs):

    single_process_count = run_command('ps -elf|grep -w {}|grep -v grep|sed \'/^ *$/d\'|wc -l'.format(name),write_logs)
    if int(single_process_count[1]) <1:
        # print('{} is down'.format(name))
        return 0

    else:
        return 1
        # print(single_process_count)


