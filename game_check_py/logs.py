#/usr/bin/env python

# -*- coding: utf-8 -*-

#by Jack

import logging
import games_check_index

def write_logs(game_name,log_dir='/tmp/'):
    logger = logging.getLogger('{}'.format(game_name))
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler(filename='{}{}_check_log_{}'.format(log_dir,game_name,games_check_index.today_date))
    fh.setLevel(logging.DEBUG)

    fmt = "%(asctime)s %(levelname)s %(message)s"
    datefmt = "%m/%d/%Y %H:%M:%S"
    formatter =logging.Formatter(fmt,datefmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger


# write_logs('mj')
