#/usr/bin/env python

#by Jack
import os

def get_list(write_logs,game_name,file_dir,file_name):
    try:
        name_list = []
        with open('{}{}'.format(file_dir,file_name),mode='r') as n:
            for name in n:
                if name.startswith('./') and len((name.split(' '))) >1:
                    name_list.append(name.split(' ')[1].strip().strip('&'))
                elif name.startswith('nohup') and name.split(' ')[1].startswith('./'):
                    name_list.append(name.split(' ')[2].strip().strip('&'))
                elif name.startswith('nohup') and (name.split(' ')[1]).startswith('../'):
                    name_list.append(name.split(' ')[1].strip().strip('&'))
                elif name.startswith('../'):
                    name_list.append(name.split(' ')[0].strip().strip('&'))

        with open('/tmp/games_name.txt', mode='w+') as game_file:
            for i in name_list:
                game_file.writelines('{}\n'.format(i))
            if name_list and game_file.readline().startswith('\n') is False:
                write_logs.info('get {}_list success...'.format(game_name))
            else:
                write_logs.error('get {}_list fail...'.format(game_name))

    except:
        write_logs.error('get {}_list error...'.format(game_name))