<?php

function getTableTimeArray($table_pattens,$number_pattens,$directory="./"){
    if(!is_dir($directory)){
      return false;
    }

    $dh=opendir($directory);
    $table_times=array();

    while(($file = readdir($dh))!== false){
        if($file=='.'||$file==".."){
            continue;
        }
	$gen_time=substr($file,-14);
	$table_name=substr($file,0,-14);
	if(empty($gen_time)||!is_numeric($gen_time)||empty($table_name)){

		continue;

	}
        //preg_match("$table_pattens",$file,$arr0)&&preg_match("$number_pattens",$file,$arr1);
        $table_times["$table_name"][]=$gen_time;

    }

    return $table_times;
}



function getMaxTimeFileNames($table_times=array()){
    $max_array=array();

    if(empty($table_times)){
        return false;
    }

    foreach($table_times as $key => $values){

        $max_array["$key"]=max($values);

    }
    //file_put_contents($file,var_export($max_array, TRUE));

    return $max_array;

}

function InitPid($pid_file,$d_mode='0755'){
	if(is_file($pid_file)){
		return true;
	}

	$dirname = dirname(rtrim($pid_file,'/'));
	if(isset($dirname) && !empty($dirname)){

		if(mkdir($dirname,$d_mode,true)){
			$handler = fopen($pid_file,"w");
			if(isset($handler) && $handler){
				fwrite($handler,"finish");

				return true;

			}

		}



	}

	return false;



}


function InitPosition($pos_file,$dmode='0755'){


        if(isset($pos_file) && !file_exists($pos_file)){


                $handler =  fopen($pos_file,"w");
                if(!isset($handler)||!$handler){
                        return false;
                }
                if(!fwrite($handler,"none")){
                        return false;

                }
                fclose($handler);

        }


	return  true;




}

//获取上次导入数据的位置（位置是时间打印的数字）
function getPreviousTimes($pos_file,$d_mode='0755'){


	$position=file_get_contents($pos_file);
	if(!isset($position)|| !$position){

		return false;

	}
	return $position;


}


//从表名中获取字段数组
function getFieldsFromTable($table,$conn){

	if(!isset($conn) ||!is_resource($conn) || !mysql_ping($conn)){
		$ret_con = false;
		for($j=0;$j<5;$j++){
			$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
			mysql_select_db("${db_config['db_name']}",$conn);
			if(isset($conn) && is_resource($conn) && mysql_ping($conn)){
				$ret_con = true;
				break;
				
			}
			sleep(1);
			
		}
        
        if(!$ret_con){

            echo "loadTxtDataIntoDatabase重连失败".PHP_EOL;
            exit;

        }
	}

    if(!empty($table)){
        $sql="show columns from $table ";
        $results=mysql_query($sql,$conn);
	if(!is_resource($results)){
		return false;

	}
        while($row=mysql_fetch_row($results)){
            if(!empty($row[5]) && preg_match('/auto_increment/',$row[5])){
                continue;
            }  
            $fields[]=$row[0];
                        
                        
      
        }
        return $fields;	
    }

    return false;

}


function getDirList($dir){
        $dirs=array();
        if(!is_dir($dir)){

                return false;

        }
        $dir_list=scandir($dir);
        foreach($dir_list as $dirfile){
		$dir_fullpath=rtrim($dir,'/').'/'.$dirfile;
                if($dirfile!='.' && $dirfile!='..' && is_dir($dir_fullpath)){
			$dirs[] = $dir_fullpath;


                }


        }


        return $dirs;

}


function getFileList($dir,$pid_file,$file_dir){
        $dirs=array();
        if(!is_dir($dir)|| !is_dir($file_dir)){
		echo "getFileList的目录不存在".PHP_EOL;

                return false;

        }
        $dir_list=scandir($dir);
	/*
	$pro_name = getProductName($pid_file);
	echo "XXXXXXXXX".$dir.$pro_name.PHP_EOL;
	if(!isset($pro_name) || !$pro_name){
		echo "产品名称不存在".PHP_EOL;
		return false;

	}
	*/
	
        foreach($dir_list as $dirfile){
                $dir_fullpath=rtrim($dir,'/').'/'.$dirfile;
		$dir_fullpath_log = rtrim($file_dir,'/').'/'.$dirfile;
		echo $dir_fullpath_log.PHP_EOL;
                if($dirfile!='.' && $dirfile!='..' && is_file($dir_fullpath) && is_dir($dir_fullpath_log)){
                        $dirs[] = $dir_fullpath;


                }


        }


        return $dirs;

}
//检查该定时任务是否已经执行

function position_recovery($pid_file,$pos_file,$log_file,$file_dir){

	//临界区
	if(!isset($pid_file) || !file_exists($pid_file)){
		echo "ERROR:pid文件不存在".PHP_EOL;
		return false;
	

	}
	$pid = file_get_contents($pid_file);
	file_put_contents($pid_file,"running");
	if(isset($pid) && $pid == "finish"){
		return true;


	}
	else{
		
		echo "WARNINGS:文件进程ID还存在，实际进程不存在，正在恢复位置中...".PHP_EOL;
		if(isset($pos_file) && !empty($pos_file) && is_dir($pos_file)){
			if(recovery_dir_position($pos_file,$log_file,$pid_file,$file_dir)){
				echo "自动修复位置成功".PHP_EOL;
				return true;

			}

		}elseif(isset($pos_file) && !empty($pos_file) && is_file($pos_file) ){
			if(recovery_position($pos_file,$log_file)){
				echo "自动修复位置成功".PHP_EOL;
				return true;
			}

		}else{

			echo "ERROR:自动修复位置失败，请手动修复".PHP_EOL;
			return false;
		}

	}


}
/*
//创建pid
function create_pid($file){
	$handler=fopen("${file}","w");
	flock($handler,LOCK_EX);
	if(!empty($handler)){
		$pid = (string)getmypid();
		$line_number = fwrite($handler,$pid);
		if(!$line_number){
			echo "写进程id值的时候失败！".PHP_EOL;
			return false;

		}
		return $handler;
	}

	return false;

}


function delete_pid($handler,$file){
	flock($handler,LOCK_UN);
	fclose($handler);
	unlink($file);
}
*/

function recovery_dir_position($dirnames,$log_file,$pid_file,$file_dir){
	if(!is_dir($dirnames)){
		echo "dirnames不是目录".PHP_EOL;
		return false;

	}
	$pos_files = getFileList($dirnames,$pid_file,$file_dir);
	if(empty($pos_files)){
		echo "位置文件不存在".PHP_EOL;
		return false;
	}
	
	foreach($pos_files as $pos_file){
		if(stristr($pos_file,$pid_file) || stristr($pos_file,'.swp') || stristr($pos_file,'recover.pid')){
			continue;


		}
		echo "开始恢复文件该文件:".$pos_file.PHP_EOL;
		if(!recovery_position($pos_file,$log_file)){
			echo $pos_file."恢复失败".PHP_EOL;
			return false;


		}


	}

	return true;




}
/*
function getProductName($pid_file){

	if(!file_exists($pid_file)){

		return false;
	}
	$dirnames = dirname(rtrim($pid_file,'/'));
	$ret = basename($dirnames);
	return $ret;



}
*/
function recovery_position($pos_file,$log_file){
	$arr_pos = array();
	echo "位置文件是".$pos_file.PHP_EOL;
	echo "日志文件是".$log_file.PHP_EOL;
	if(!file_exists($pos_file) || !file_exists($log_file)){
		echo "文件不存在".PHP_EOL;
		return false;

	}	
	$positions = get_pos_from_file($pos_file);
	echo "修复前的位置为:".PHP_EOL;
	print_r($positions);
	if(empty($positions)){
		echo "修复空位置成功".PHP_EOL;
		return true;

	}
	
	$handler = fopen("${log_file}","r+");
	if(!$handler){
		echo "ERROR:日志文件打开失败".PHP_EOL;

	}
	$offset = -1;
	$line = '';
	//for($i=0;$i<$rows;$i++){
	$arr_pos =array();
	while( fseek($handler,$offset,SEEK_END)>=0 ){
		$ch = fgetc($handler);
		$offset --;
		if($ch=="\n"){

			$line=fgets($handler);
			$lines = explode('|',$line);
			if(count($lines) <3){
				continue;
			}
			$relate_dirs=explode('/',$pos_file);
			if(empty($positions)){
	
				break;

			}
			$pos_dir_names = explode('/',$lines[1]);
			if(is_array($positions) && !empty($positions)){
				foreach($positions as $table => $key){
					if( strstr($lines[1],$table) && stristr($lines[2],"Success") 
					&& ( stristr($lines[1],$relate_dirs[count($relate_dirs)-2]) && $pos_dir_names[count($pos_dir_names)-2] == $relate_dirs[count($relate_dirs)-1] 
					||stristr($relate_dirs[count($relate_dirs)-2],"product") && stristr($relate_dirs[count($relate_dirs)-1],"pos") )   ){
						$add = 0;
						if((int)substr($lines[1],-1) == 0){
							$add =1;

						}	
						//$positions["$table"] = (int)substr($lines[1],-14) + $add ;
						$arr_pos["$table"] = (int)substr($lines[1],-14) + $add ;
						unset($positions["$table"]);			


					} 
			
				}			

			}else{

				if( strstr($lines[0],"load") && stristr($lines[2],"Success") 
				&& ( stristr($lines[1],$relate_dirs[count($relate_dirs)-2]) && $pos_dir_names[count($pos_dir_names)-2] == $relate_dirs[count($relate_dirs)-1] 
				||stristr($relate_dirs[count($relate_dirs)-2],"product") && stristr($relate_dirs[count($relate_dirs)-1],"pos") )   ){
					$table_g = substr($pos_dir_names[count($pos_dir_names)-1],0,-14);
					$values = (int)substr($pos_dir_names[count($pos_dir_names)-1],-14)+1;

					if(empty($arr_pos["$table_g"])){

						$arr_pos["$table_g"] = $values;

					}
			
				}


			}	

		}
	}
	if(is_array($positions) && !empty($positions)){
		foreach($positions as $key => $value){
			 $arr_pos["$key"]=$value;

		}

	
	}
	if(empty($arr_pos)){

		$arr_pos = "none";

	}
	echo "修复后的位置为:".PHP_EOL;
	if(is_array($arr_pos)){
		print_r($arr_pos);

	}
	else{
		echo $arr_pos.PHP_EOL;
	}
	if(!write_positions($pos_file,$arr_pos)){
		echo "写恢复位置失败".PHP_EOL;
		return false;

	}
	echo "修复位置成功".PHP_EOL;
	return true;

}

function get_pos_from_file($pos_file){
	$get_position=file_get_contents($pos_file);

	if(!isset($get_position) ||  !$get_position || empty($get_position)){
		echo "ERROR:pos_file为空或读取出错".PHP_EOL;
		exit;


	}

	echo $pos_file;
	if(stristr($get_position,"array")){
		$array_position = '$positions='.$get_position.';';
		eval("$array_position");
	}else{

		$positions = $get_position;

	}
	return $positions;


}


function write_positions($pos_file,&$positions){

	$n_times=1;
	while($n_times>0){
        	if(!file_put_contents($pos_file,is_array($positions)?var_export($positions, TRUE):$positions)){
                	sleep(5);
               		 echo "put position waitting...".PHP_EOL;
			$n_times ++;
			if($n_times >100){
				return false;

			}
               		continue;

        	}
       	        break;

	}
	return true;

}


function get_rows($log_file){
	$lines = 0;
	if ($fh = fopen($log_file,'r')) {
		while (! feof($fh)) {
			if (fgets($fh)) {
				$lines++;
			}
		}
		fclose($fh);
		return $lines;
	}
	return false;

}

//检查该定时任务是否已经执行
function check_proc_run($key="u3d1.log",$critical_value=2){
        $shell_cmd = "ps -ef | grep $key| grep -v grep |wc -l";

        exec($shell_cmd,$out,$ret_status);
        if($ret_status == 0 && $out[0] >= $critical_value){
                return true;

        }
        return false;


}
