<?php
error_reporting(E_ALL & ~E_NOTICE);
require 'db_config.php';
require 'import_new.php';
require 'need_file_new.php';
//array_shift($agrv);
date_default_timezone_set('Etc/GMT-8');
$dir="$argv[1]";
$splitChar = '|';  //竖线
$allow_time=1200;
$table_pattens='/^[a-zA-Z]*/';
$number_pattens='/[1-9][0-9]*$/';

$conn=mysql_connect("${db_config['db_hostname']}:${db_config['db_port']}","${db_config['db_user']}","${db_config['db_password']}");
mysql_select_db("${db_config['db_name']}",$conn);

$position_dir='/app/script/php/two/';
//需要导入的数据目录,这里针对一级子目录
if(!empty($argv[3])){
	$position_dir="$argv[3]";
}


//获得当前时间 
$cur_system_time = time();
if(empty($cur_system_time)){
	echo "failed:获取时间戳失败！！".PHP_EOL;
	exit;
}


$division = substr(rtrim($position_dir,'/'),-2);
if(!is_numeric($division)){
	$division = substr(rtrim($position_dir,'/'),-1);
}

//关键词，如果关键词大于等于1说明当前脚本在运行
$key_part = "$argv[2]";
//求得PID文件
$pid_file=rtrim($position_dir,'/').'/'.$key_part;
$log_file=dirname(rtrim($position_dir,'/')).'/log/load_log'.$division.'.log';
$pos_files=rtrim($position_dir,'/').'/';

if(check_proc_run($dir,5)){
	exit;
}


if(!is_dir(dirname(ltrim($pid_file,'/')))){
	if(!InitPid($pid_file)){
		echo "初始化进程ID文件失败".PHP_EOL;
	}
}


//进程上锁
$handler = fopen($pid_file,"r");
if(!$handler){
	echo "进程预上锁失败，程序退出".PHP_EOL;

}
flock($handler,LOCK_EX);


$dirs= getDirList($dir);
foreach($dirs as $posdir){
	$pos_fl=basename(rtrim($posdir,'/'));
	if(!InitPosition(rtrim($pos_files.$pos_fl,'/'))){
		exit;
	}
}

if(!position_recovery($pid_file,$pos_files,$log_file,$dir)){
	exit;
}


foreach($dirs as $dirname){

	//获取已经导入数据文件的位置，如果没配置文件，则自动创建
	$pos_file_name = basename(rtrim($dirname,'/'));
	$get_position = getPreviousTimes(rtrim($pos_files.$pos_file_name,'/'));
	if(!isset($get_position) || !$get_position){
		echo "获取位置存在问题".PHP_EOL;
		exit;
	}
	
	//print_r($get_position);
	if($get_position){
        if($get_position == "none"){
			$positions = array();
        }elseif(stristr($get_position,"array")){
			$str_array_position = '$positions='.$get_position.';';
			eval("$str_array_position");
		}else{
			echo $pos_file_name.$get_position."不规范的位置结构".PHP_EOL;
			exit;
		}
	}

	//获取一个数组，是表名与时间的一个对应关系
	//对于一个日志文件，最新的文件可能在写入，所以不能导入
	$table_to_times = getTableTimeArray($table_pattens,$number_pattens,$dirname);
	//exit;
	if(!$table_to_times || empty($table_to_times)){
		echo "文件不存在或者获取日志失败".PHP_EOL;
		continue;
	}
	
	//获取最新日志文件的时间值，用于比较，拥有新时间值得不会导入，因为该文件可能在写入
	$max_names = getMaxTimeFileNames($table_to_times);
	foreach($max_names as $table => $number){
	    
		$fields=getFieldsFromTable($table,$conn);
		if($fields == "1146"){
			echo "$table 在数据库中不存在".PHP_EOL;
			continue;
		}
		
		//检查表结构
		if(!$fields || !is_array($fields))
		{
			echo "$table 在数据库中不存在".PHP_EOL;
			continue;
		}
		
		$last_file_time = strtotime($number);
		$intervals = $cur_system_time-$last_file_time;
		if(empty($last_file_time)&&!is_numeric($last_file_time)||$intervals < 0){
			$intervals=$allow_time-1;
		}

		foreach($table_to_times as $key => $values){
		
	        if($table == $key){
				//判断数据是否有变化
				$values = array_unique($values); 
				sort($values);
				$change=0;
        	   	
				for($i=0;$i<count($values);$i++){
				 //如果一个文件的位置在位置文件中以及存在，则用前者来导入数据，否则用后者
					if((!empty($positions["$table"]) && $values["$i"] >= $positions["$table"] || empty($positions["$table"]) )  && ($values["$i"] < $number||($values["$i"]==$number&&$intervals>$allow_time)) ){
						$file=$table.$values["$i"];
						echo "开始导入文件: $file".PHP_EOL;
						
						$result = loadTxtDataIntoDatabase($pid_file,$splitChar,rtrim($dirname,'/').'/'.$file,$table,$conn,$fields);
						
						if (!empty($result)&&array_shift($result)){
							echo 'load|'.$dirname.'/'.$file.'|Success!'."导入时间是:".date('y-m-d h:i:s',time()).PHP_EOL;
							//如果有load数据，说明位置有变化
							if(!$change){
								$change=1;
							}
							if($values["$i"]==$number&&$intervals > $allow_time){
								$max_names["$table"]=$values["$i"]+1;
							}
						}
						else{
							$max_names["$table"]=$values["$i"];
							$content="load|".$dirname.'/'.$file."|failed!<br \>"."导入时间是:".date('y-m-d h:i:s',time()).PHP_EOL;
							//sendMail($content);
							//sendWechat($content);
							echo $content.PHP_EOL;
							break;
						}
					}
		    
				}
				//位置不为空且位置没变化，所以还是原来的位置
				if( !empty($positions["$table"]) && $change==0 ){
					$max_names["$table"] = $positions["$table"];
				}
			}
		}
	}

	$n_times=1;
	while($n_times>0){
		$pos_string = var_export($max_names, TRUE);
		if(!file_put_contents($pos_files.$pos_file_name,$pos_string)){
			sleep(5);
			echo "put position waitting...".PHP_EOL;
			continue;

		}
		break;

	}
}
$log_rows = get_rows($log_file);

if(isset($log_rows) && is_numeric($log_rows) && $log_rows>300000 ){
	$rename_file=$log_file. date("YmdHis");
	rename($log_file,$rename_file);
}


//解除进程锁
file_put_contents($pid_file,"finish");
flock($handler,LOCK_UN);
