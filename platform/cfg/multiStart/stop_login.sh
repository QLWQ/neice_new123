#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep -v grep | grep .xml | grep login | grep $runpath |  awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done
