#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep -v grep | grep .xml | grep mail |grep $runpath |  awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done
