#!/bin/bash

ssh db1 nohup tail -f /app/game/platform/log/log/dwh/* >> dwh.log 2>&1 & 
ssh db2 nohup tail -f /app/game/platform/log/log/dwh/* >> dwh.log 2>&1 & 
ssh db3 nohup tail -f /app/game/platform/log/log/dwh/* >> dwh.log 2>&1 &
ssh db4 nohup tail -f /app/game/platform/log/log/dwh/* >> dwh.log 2>&1 &
