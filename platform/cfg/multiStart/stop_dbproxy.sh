#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep -v grep | grep .xml | grep dbproxy | grep $runpath |  awk '{print $2}'`
for PID in $PIDS
do
	kill $PID
done
