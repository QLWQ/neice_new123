#!/bin/bash

runpath=$PWD

PIDS=`ps -ef | grep -v grep | grep tail | grep nohup | grep login | awk '{print $2}'`
for PID in $PIDS
do
	kill -9 $PID
done

rm -rf ./login.log
