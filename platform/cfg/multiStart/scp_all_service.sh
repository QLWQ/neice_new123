#!/bin/bash

scp -r /app/game/platform web:/app/game/
scp -r /app/game/platform login:/app/game/
scp -r /app/game/platform dbp:/app/game/
scp -r /app/game/platform dwh:/app/game/
scp -r /app/game/platform db1:/app/game/
scp -r /app/game/platform db2:/app/game/
scp -r /app/game/platform db3:/app/game/
scp -r /app/game/platform db4:/app/game/
