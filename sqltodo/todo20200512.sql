
-- alter table `t_relation` drop column `t_path`;
alter table `t_relation` add column `t_promoteTime` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '推广时间' after t_path;

DROP PROCEDURE IF EXISTS PROC_SPLIT_TABLE;
CREATE PROCEDURE `PROC_SPLIT_TABLE`(IN `dbName` varchar(64), IN `tableName` varchar(64), IN `renameTableName` varchar(64))
BEGIN
	--Routine body goes here...
	--DECLARE split_sql text;
	--DECLARE done INT DEFAULT 0;  

	--SET @split_sql = CONCAT(
	--"IF NOT EXISTS (SELECT 1 FROM information_schema.TABLES WHERE TABLE_NAME = '", renameTableName, "') THEN",
	--"	RENAME TABLE '", tableName, "' to '", renameTableName, "';",
	--"	CREATE TABLE '", tableName, "' like '", renameTableName, "';",
	--"	CALL PROC_SYN_VIEW('", tableName, "');"
	--"END IF;");
	
	set @done = 0;
	SET @check_sql =  CONCAT("SELECT 1 INTO @done FROM information_schema.TABLES WHERE TABLE_SCHEMA = '", dbName, "' AND TABLE_NAME = '", renameTableName, "' LIMIT 1;");
	SET @rename_sql = CONCAT("RENAME TABLE ", tableName, " to ", renameTableName, ";");
	SET @create_sql = CONCAT("CREATE TABLE ", tableName, " like ", renameTableName, ";");
	SET @view_sql =   CONCAT("CALL PROC_SYN_VIEW('", tableName, "');");
	--SELECT @check_sql;

	PREPARE stmt FROM @check_sql;
	EXECUTE stmt; 
	DEALLOCATE PREPARE stmt;
	--select @done;

	IF ISNULL(@done) or @done = 0 THEN
		--select @rename_sql;
		PREPARE stmt1 FROM @rename_sql;   
		EXECUTE stmt1; 
		DEALLOCATE PREPARE stmt1; 

		--select @create_sql;
		PREPARE stmt2 FROM @create_sql;   
		EXECUTE stmt2; 
		DEALLOCATE PREPARE stmt2; 

		--select @view_sql;
		PREPARE stmt3 FROM @view_sql;   
		EXECUTE stmt3; 
		DEALLOCATE PREPARE stmt3;
	--ELSE
	--	SELECT @check_sql, @rename_sql, @create_sql, @view_sql;
	END IF;

	--IF NOT EXISTS (SELECT 1 FROM information_schema.TABLES WHERE TABLE_NAME = renameTableName) THEN
	--	RENAME TABLE tableName to renameTableName;
	--	CREATE TABLE tableName like renameTableName;
	--	CALL PROC_SYN_VIEW(tableName);
	--END IF;
END