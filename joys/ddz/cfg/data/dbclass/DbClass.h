#ifndef __DBCLASS_H__
#define __DBCLASS_H__
#include "BaseStdInt.h"
#include "DbOpt.h"

extern std::vector<std::string> gCacheTblNames;	// global cfg var

class	CDB_Ddz{
public:
	CDB_Ddz(){}
	~CDB_Ddz(){}
	static uint32_t	GetVirDbId(){ return 1;}
	static void Init();
	static const std::vector<CDBFieldDef>& GetFieldDefs();
	static uint16_t Name2Idx(const std::string& def);
	static const std::string& Idx2Name(uint16_t idx);
	static CDBBaseOpt*	insert(std::string key , std::string value);
	static CDBBaseOpt*	select_withkey( std::string key , bool flag_key = true , bool flag_value = true);
	static CDBBaseOpt*	update_withkey(std::string key , std::string* value = NULL);
	static CDBBaseOpt*	replaceInto(std::string key , std::string value);
	static CDBBaseOpt*	delete_withkey( std::string key);
	static CDBBaseOpt*	select_withclumn_and( std::string* key = NULL , std::string* value = NULL , bool flag_key = true , bool flag_value = true);
	static CDBBaseOpt*	select_withclumn_or( std::string* key = NULL , std::string* value = NULL , bool flag_key = true , bool flag_value = true);
	static CDBBaseOpt*	select_count_withand( std::string* key = NULL , std::string* value = NULL);
	static CDBBaseOpt*	select_count_withor( std::string* key = NULL , std::string* value = NULL);
	static CDBBaseOpt*	select_colum_maxvalue( std::string columName , std::string* key = NULL , std::string* value = NULL);
	static CDBBaseOpt*	exe_cmd( int8_t blob , int32_t resultType , const std::string& sqlStr , const std::string& cacheId , const std::string& cacheParam , const std::vector<CDBFieldDef>& columnDefs);
	static CDBBaseOpt*	sel_topn_withclumn_and(uint32_t selLimitCnt, std::string* key = NULL , std::string* value = NULL , bool flag_key = true , bool flag_value = true);
	static CDBBaseOpt*	sel_topn_withclumn_or(uint32_t selLimitCnt, std::string* key = NULL , std::string* value = NULL , bool flag_key = true , bool flag_value = true);
	
public:
	static std::string m_tableName;
	static std::vector<CDBFieldDef> m_vectColumnDefs;
	static std::map<std::string,uint16_t> m_name2Idx;
	static bool m_bIsCacheTbl;
	static std::pair<int16_t, int16_t> m_pairKeyIdx; // index start from 0

};
void GlobalTblClsInit();
const std::string& Idx2Name(const std::string& tblName,uint16_t idx);
uint16_t Name2Idx(const std::string& tblName, const std::string& name);
#endif //__DBCLASS_H__
