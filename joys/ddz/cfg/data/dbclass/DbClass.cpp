#include<sstream>
#include<iostream>
#include<assert.h>
#include "DbClass.h"

std::vector<std::string> gCacheTblNames;


std::string CDB_Ddz::m_tableName = "t_ddz";
std::vector<CDBFieldDef> CDB_Ddz::m_vectColumnDefs;
std::map<std::string,uint16_t> CDB_Ddz::m_name2Idx;
bool CDB_Ddz::m_bIsCacheTbl = false;
std::pair<int16_t, int16_t> CDB_Ddz::m_pairKeyIdx = std::make_pair(0,-1);

void	CDB_Ddz::Init()
{
	if(m_vectColumnDefs.empty())
	{
		CDBFieldDef	_1_field;	
		_1_field.Init("t_key",DBVAR_STR,(uint16_t)m_vectColumnDefs.size());		
		m_vectColumnDefs.push_back(_1_field);
		CDBFieldDef	_2_field;	
		_2_field.Init("t_value",DBVAR_BLOB,(uint16_t)m_vectColumnDefs.size());		
		m_vectColumnDefs.push_back(_2_field);
	
		for(size_t i = 0; i < m_vectColumnDefs.size();++i)
		{
			m_name2Idx[m_vectColumnDefs[i].m_fieldName] = (uint16_t)i;
		}
		
		m_bIsCacheTbl = false;
	}
}	
const std::vector<CDBFieldDef>&	CDB_Ddz::GetFieldDefs()
{	
	return m_vectColumnDefs;
}	
uint16_t	CDB_Ddz::Name2Idx(const std::string& def)
{	
	return m_name2Idx[def];	
}	
const std::string& CDB_Ddz:: Idx2Name(uint16_t idx)
{		
	return m_vectColumnDefs[idx].m_fieldName;
}	
CDBBaseOpt*	CDB_Ddz::insert( std::string key , std::string value)
{
	CDBOpt_Insert* pOpt = static_cast<CDBOpt_Insert*>(CDBBaseOpt::CreateOptObj(OPTTYPE_INSERT));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey<<key;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "t_ddz:" +  pOpt->m_cacheKey;
	
	CDBFieldInfo	key_info;
	key_info.m_fieldName	=	"t_key";
	key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
	key_info.m_fieldValue.SetValue(key);
	pOpt->m_fieldInfos.push_back(key_info);
	
	CDBFieldInfo	value_info;
	value_info.m_fieldName	=	"t_value";
	value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
	value_info.m_fieldValue.SetBlob(value);
	pOpt->m_fieldInfos.push_back(value_info);
	return pOpt;
}
CDBBaseOpt*	CDB_Ddz::select_withkey( std::string key , bool flag_key , bool flag_value)
{
	if(!flag_key &&!flag_value)
		return nullptr;
	CDBOpt_Sel_WithKey* pOpt = static_cast<CDBOpt_Sel_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey<<key;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "t_ddz:" +  pOpt->m_cacheKey;
	
	CDBFieldInfo	key_info;
	key_info.m_fieldName	=	"t_key";
	key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
	key_info.m_fieldValue.SetValue(key);
	pOpt->m_keyFieldInfos.push_back(key_info);
	if(flag_key)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_key";
		fieldDef.m_fieldType	=	DBVAR_STR;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	if(flag_value)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_value";
		fieldDef.m_fieldType	=	DBVAR_BLOB;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	return pOpt;
}
CDBBaseOpt*	CDB_Ddz::update_withkey(std::string key , std::string* value)
{
	if(!value)
		return nullptr;
	CDBOpt_Update_WithKey* pOpt = static_cast<CDBOpt_Update_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_UPDATE_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey<<key;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "t_ddz:" +  pOpt->m_cacheKey;
	
	CDBFieldInfo	key_info;
	key_info.m_fieldName	=	"t_key";
	key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
	key_info.m_fieldValue.SetValue(key);
	pOpt->m_keyFieldInfos.push_back(key_info);
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_valueFieldInfos.push_back(value_info);
	}
	return pOpt;
}
CDBBaseOpt*	CDB_Ddz::replaceInto( std::string key , std::string value)
{
	CDBOpt_ReplaceInto* pOpt = static_cast<CDBOpt_ReplaceInto*>(CDBBaseOpt::CreateOptObj(OPTTYPE_REPLACEINTO));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey<<key;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "t_ddz:" +  pOpt->m_cacheKey;
	
	CDBFieldInfo	key_info;
	key_info.m_fieldName	=	"t_key";
	key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
	key_info.m_fieldValue.SetValue(key);
	pOpt->m_fieldInfos.push_back(key_info);
	
	CDBFieldInfo	value_info;
	value_info.m_fieldName	=	"t_value";
	value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
	value_info.m_fieldValue.SetBlob(value);
	pOpt->m_fieldInfos.push_back(value_info);
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::delete_withkey( std::string key)
{
	CDBOpt_Del_WithKey* pOpt = static_cast<CDBOpt_Del_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_DELETE_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey<<key;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "t_ddz:" +  pOpt->m_cacheKey;
	
	CDBFieldInfo	key_info;
	key_info.m_fieldName	=	"t_key";
	key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
	key_info.m_fieldValue.SetValue(key);
	pOpt->m_keyFieldInfos.push_back(key_info);
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::select_withclumn_and( std::string* key , std::string* value , bool flag_key , bool flag_value)
{
		
	if(!key &&!value)
		return nullptr;
	if(!flag_key &&!flag_value)
		return nullptr;
	CDBOpt_Sel_WithClumnAnd* pOpt = static_cast<CDBOpt_Sel_WithClumnAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHCLUMNAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_keyFieldInfos.push_back(key_info);
	}
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_keyFieldInfos.push_back(value_info);
	}
	if(flag_key)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_key";
		fieldDef.m_fieldType	=	DBVAR_STR;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	if(flag_value)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_value";
		fieldDef.m_fieldType	=	DBVAR_BLOB;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::select_withclumn_or( std::string* key , std::string* value , bool flag_key , bool flag_value)
{
	
	if(!key &&!value)
		return nullptr;
	if(!flag_key &&!flag_value)
		return nullptr;
	CDBOpt_Sel_WithClumnOr* pOpt = static_cast<CDBOpt_Sel_WithClumnOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHCLUMNOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_keyFieldInfos.push_back(key_info);
	}
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_keyFieldInfos.push_back(value_info);
	}
	if(flag_key)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_key";
		fieldDef.m_fieldType	=	DBVAR_STR;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	if(flag_value)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_value";
		fieldDef.m_fieldType	=	DBVAR_BLOB;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::select_count_withand( std::string* key , std::string* value)
{
		
	if(!key &&!value)
		return nullptr;
	
	CDBOpt_Sel_Count_WithAnd* pOpt = static_cast<CDBOpt_Sel_Count_WithAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_COUNT_WITHAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
	
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_valueFieldInfos.push_back(key_info);
	}	
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_valueFieldInfos.push_back(value_info);
	}	
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::select_count_withor( std::string* key , std::string* value)
{
		
	if(!key &&!value)
		return nullptr;
	

	CDBOpt_Sel_Count_WithOr* pOpt = static_cast<CDBOpt_Sel_Count_WithOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_COUNT_WITHOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
	
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_valueFieldInfos.push_back(key_info);
	}	
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_valueFieldInfos.push_back(value_info);
	}	
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::select_colum_maxvalue( std::string columName , std::string* key , std::string* value)
{
	

	CDBOpt_Sel_MaxValue* pOpt = static_cast<CDBOpt_Sel_MaxValue*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_MAXVALUE));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_columnName	=	columName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
	
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_valueFieldInfos.push_back(key_info);
	}	
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_valueFieldInfos.push_back(value_info);
	}
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::exe_cmd( int8_t blob , int32_t resultType , const std::string& sqlStr , const std::string& cacheId , const std::string& cacheParam , const std::vector<CDBFieldDef>& columnDefs)
{
	
	CDBOpt_Exe_Cmd* pOpt = static_cast<CDBOpt_Exe_Cmd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_EXE_CMD));
	pOpt->m_tableName = m_tableName;
	pOpt->m_resultType	=	resultType;
	pOpt->m_blob			=	blob;
	pOpt->m_sqlStr		=	sqlStr;
	pOpt->m_cacheId		=	cacheId;
	pOpt->m_cacheParam	=	cacheParam;
	pOpt->m_columnDefs.assign(columnDefs.begin(), columnDefs.end());
	for (auto iter = pOpt->m_columnDefs.begin(); iter != pOpt->m_columnDefs.end(); ++iter)
	{
		iter->m_nameIdx = m_name2Idx[iter->m_fieldName];
	}
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::sel_topn_withclumn_and(uint32_t selLimitCnt, std::string* key , std::string* value , bool flag_key , bool flag_value)
{
		
	if(!key &&!value)
		return nullptr;
	if(!flag_key &&!flag_value)
		return nullptr;
	CDBOpt_SelTopN_WithClumnAnd* pOpt = static_cast<CDBOpt_SelTopN_WithClumnAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECTTOPN_WITHCLUMNAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	pOpt->m_nSelLimitCnt	= selLimitCnt;
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_keyFieldInfos.push_back(key_info);
	}
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_keyFieldInfos.push_back(value_info);
	}
	if(flag_key)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_key";
		fieldDef.m_fieldType	=	DBVAR_STR;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	if(flag_value)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_value";
		fieldDef.m_fieldType	=	DBVAR_BLOB;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	return pOpt;
}

CDBBaseOpt*	CDB_Ddz::sel_topn_withclumn_or(uint32_t selLimitCnt, std::string* key , std::string* value , bool flag_key , bool flag_value)
{
		
	if(!key &&!value)
		return nullptr;
	if(!flag_key &&!flag_value)
		return nullptr;
	CDBOpt_SelTopN_WithClumnOr* pOpt = static_cast<CDBOpt_SelTopN_WithClumnOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECTTOPN_WITHCLUMNOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	pOpt->m_nSelLimitCnt	= selLimitCnt;
	if(key)
	{
		CDBFieldInfo	key_info;
		key_info.m_fieldName	=	"t_key";
		key_info.m_nameIdx	=	m_name2Idx[key_info.m_fieldName];
		key_info.m_fieldValue.SetValue(*key);
		pOpt->m_keyFieldInfos.push_back(key_info);
	}
	if(value)
	{
		CDBFieldInfo	value_info;
		value_info.m_fieldName	=	"t_value";
		value_info.m_nameIdx	=	m_name2Idx[value_info.m_fieldName];
		value_info.m_fieldValue.SetBlob(*value);
		pOpt->m_keyFieldInfos.push_back(value_info);
	}
	if(flag_key)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_key";
		fieldDef.m_fieldType	=	DBVAR_STR;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	if(flag_value)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"t_value";
		fieldDef.m_fieldType	=	DBVAR_BLOB;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
	return pOpt;
}
void GlobalTblClsInit()
{
	CDB_Ddz::Init();
	if(CDB_Ddz::m_bIsCacheTbl)
	{
		gCacheTblNames.push_back(CDB_Ddz::m_tableName);
	}
	
	
}
const std::string& Idx2Name(const std::string& tblName,uint16_t idx)
{
	const char* strTblName = tblName.c_str();
	if(strcmp(strTblName,"t_ddz") == 0)
	{
		return CDB_Ddz::Idx2Name(idx);				
	}
	static std::string str = "";
	return str;
}
uint16_t Name2Idx(const std::string& tblName, const std::string& name)
{
	const char* strTblName = tblName.c_str();
	if(strcmp(strTblName,"t_ddz") == 0)
	{
		return CDB_Ddz::Name2Idx(name);				
	}
	return 0;
}
