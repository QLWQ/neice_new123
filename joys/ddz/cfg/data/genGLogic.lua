
require "config"

local code = [[
#include "GameLogic.h"
#include "DbProxyService.h"
#include "DbClass.h"
#include "GameTask.h"

CGameHandler::CGameHandler()
{
}

CGameHandler::~CGameHandler()
{
}

BEGIN_MESSAGE_MAP(CGameHandler, CBaseHandler)


	ON_NET_MESSAGE(SS_H2D_LOADGAMEDATA_REQ, On_H2D_LoadGameData_Req)
	ON_NET_MESSAGE(SS_H2D_SAVEGAMEDATA_NTY, On_H2D_SaveGameData_Nty)
	ON_NET_MESSAGE(SS_H2D_SAVEGAMEDATA_REQ, On_H2D_SaveGameData_Req)


END_MESSAGE_MAP()

void CGameHandler::On_H2D_LoadGameData_Req(CBaseService * pBaseService, uint32_t srvId, uint16_t srvType, uint32_t sessId, bool smallEndian, uint32_t srcServiceId, CBaseNetMsg * pNetMsg)
{
	CDbProxyService*pService = (CDbProxyService*)pBaseService;
	SS_H2D_LoadGameData_Req* pReq = static_cast<SS_H2D_LoadGameData_Req*>(pNetMsg);
	
	uint32_t nLoadDbID;
	CDBBaseOpt* pOpt = nullptr;		
	table_map_load_code		
	if (pOpt)
	{
		CTask_LoadGameData* pTask = pService->m_taskAlloctor.AllocObj<CTask_LoadGameData>();
		if (!pTask)
		{
			delete pOpt;
			return;
		}	
		pTask->Init(GetTimeTick(), pService->GenTaskId(), srcServiceId, *pReq);
		pTask->AddNewOpt(pOpt, nLoadDbID);
		pService->AddServiceTask(pTask);
		pTask->CommitOpts(pService);
	}
	else
	{
		log_error_app("[%s:%d] hanle fail tbl name=%s\n", __FILE__, __LINE__, pReq->m_tableName.c_str());
	}
}

void CGameHandler::On_H2D_SaveGameData_Nty(CBaseService * pBaseService, uint32_t srvId, uint16_t srvType, uint32_t sessId, bool smallEndian, uint32_t srcServiceId, CBaseNetMsg * pNetMsg)
{
	CDbProxyService*pService = (CDbProxyService*)pBaseService;
	SS_H2D_SaveGameData_Nty* pReq = static_cast<SS_H2D_SaveGameData_Nty*>(pNetMsg);
	
	uint32_t nLoadDbID;
	CDBBaseOpt* pOpt = nullptr;		
	table_map_update_code
	if (pOpt)
	{
		CTask_SaveGameData* pTask = pService->m_taskAlloctor.AllocObj<CTask_SaveGameData>();
		if (!pTask)
		{
			delete pOpt;
			return;
		}	
		pTask->Init(GetTimeTick(), pService->GenTaskId(), srcServiceId);
		pTask->AddNewOpt(pOpt, nLoadDbID);
		pService->AddServiceTask(pTask);
		pTask->CommitOpts(pService);
	}
	else
	{
		log_error_app("[%s:%d] hanle fail tbl name=%s\n", __FILE__, __LINE__, pReq->m_tableName.c_str());
	}
}

void CGameHandler::On_H2D_SaveGameData_Req(CBaseService * pBaseService, uint32_t srvId, uint16_t srvType, uint32_t sessId, bool smallEndian, uint32_t srcServiceId, CBaseNetMsg * pNetMsg)
{
	CDbProxyService*pService = (CDbProxyService*)pBaseService;
	SS_H2D_SaveGameData_Req* pReq = static_cast<SS_H2D_SaveGameData_Req*>(pNetMsg);

	uint32_t nLoadDbID;
	CDBBaseOpt* pOpt = nullptr;
	table_map_update_code
	if (pOpt)
	{
		CTask_SaveGameData_Req* pTask = pService->m_taskAlloctor.AllocObj<CTask_SaveGameData_Req>();
		if (!pTask)
		{
			delete pOpt;
			return;
		}
		pTask->Init(GetTimeTick(), pService->GenTaskId(), srcServiceId, *pReq);
		pTask->AddNewOpt(pOpt, nLoadDbID);
		pService->AddServiceTask(pTask);
		pTask->CommitOpts(pService);
	}
	else
	{
		log_error_app("[%s:%d] hanle fail tbl name=%s\n", __FILE__, __LINE__, pReq->m_tableName.c_str());
	}
}
]]


local var = [[
	else if (0 == strcmp(pReq->m_tableName.c_str(),lua_tbl_name))
	{
		if ((!pService->GetGlobalTableDbIdByVirDbId(lua_tbl_cls::GetVirDbId(), nLoadDbID)) || (0 == nLoadDbID))
			return;		
		pOpt = lua_tbl_cls::select_withkey(pReq->m_key);
	}
]]

local update_var = [[
	else if (0 == strcmp(pReq->m_tableName.c_str(),lua_tbl_name))
	{
		if ((!pService->GetGlobalTableDbIdByVirDbId(lua_tbl_cls::GetVirDbId(), nLoadDbID)) || (0 == nLoadDbID))
			return;	
		pOpt = lua_tbl_cls::replaceInto( pReq->m_key,pReq->m_value);
	}
]]


local function main()
	local load_str = ""
	local update_str = ""
	local temp = ""
	for k,v in pairs(cfg) do
		k = [["]]..k..[["]]
		temp =  string.gsub(var,"lua_tbl_name",k)
		temp =  string.gsub(temp,"lua_tbl_cls",v)	
		load_str = "\n"..load_str..temp

		temp =  string.gsub(update_var,"lua_tbl_name",k)
		temp =  string.gsub(temp,"lua_tbl_cls",v)
		update_str = "\n"..update_str..temp			
	end
	load_str = string.gsub(load_str,"else if","if",1)	
	code = string.gsub(code,"table_map_load_code",load_str)
	
	update_str = string.gsub(update_str,"else if","if",1)	
	code = string.gsub(code,"table_map_update_code",update_str)
	--print(code)
	
	local output_path = "./GameLogic.cpp"
	local f = assert(io.open(output_path, 'w+'))    
	f:write(code)    
	f:close()	
end


main()

	
	
	
	
	