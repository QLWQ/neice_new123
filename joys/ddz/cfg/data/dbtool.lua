local __PATH__ = "."
function dumpTable(t ,tab)
	if tab == nil then
		tab = 1
	end
	local knum = 0;
	for k , v in pairs(t) do
		knum = knum + 1
		if type(v) == "table" then
			if v == nil then
				print(string.rep("	", tab)..k.."=nil\n")
			else
				print(string.rep("	", tab)..k.."=\n")
				dumpTable(v,tab+1)			
			end
		else
			if type(v) == "string" and string.len(v) == 0 then
				print(string.rep("	", tab)..k.."=''\n")
			elseif type(v) == "boolean" then
				if v == true then
					print(string.rep("	", tab)..k.."=true\n")
				else
					print(string.rep("	", tab)..k.."=false\n")
				end
			else
				print(string.rep("	", tab)..k.."="..v.."\n")
			end
		end
	end
	
	if type(t) == "table" and knum == 0 then
		print(string.rep("	", tab).."{}\n")
	end
end

function toBoolean(str)
	if type(str) == "string" and string.upper(str) == "TRUE" then
		return true
	end
	return false
end

--获取路径
function stripfilename(filename)
	 -- return string.match(filename, "(.+)/[^/]*%.%w+$") 	--*nix system
	 return string.match(filename,"(.+)\\[^\\]*%.%w+$") 	-- windows
 end
 
--获取文件名
function strippath(filename)
	 return string.match(filename, ".+/([^/]*%.%w+)$") -- *nix system
	 --return string.match(filename, “.+\\([^\\]*%.%w+)$”) — *windows
 end
 
--去除扩展名
function stripextension(filename)
	 local idx = filename:match(".+()%.%w+$")
	 if(idx) then
			 return filename:sub(1, idx-1)
	 else
			 return filename
	 end
 end
 
--获取扩展名
function getextension(filename)
         return filename:match(".+%.(%w+)$")
 end 

require("LuaXml")
local dbmap = xml.load("./dbmap.xml")

local dbcolumn		=	{}
function dbcolumn:new (o)
	o = o or {
		column_name 		= 	"",
		column_length		=	0,
		column_notnull 		= 	false,
		column_primarykey	= 	false,
		column_index		=	false,
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

local dbcache		=	{}
function dbcache:new (o)
	o = o or {
		cache_emergent	=	0,
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

local dbproperty	=	{
}
function dbproperty:new (o)
	o = o or {
		dbpro_name				=	"",
		dbpro_type				=	0,
		dbpro_default			=	nil,
		dbpro_des				=	"",
		dbpro_column			=	nil,
		dbpro_cache				=	nil,
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

local dbclass	=	{}

function dbclass:new (o)
	o = o or {
		db_name					=	"",
		db_table				=	"",
		db_vdbId				=	0,
		db_cacheId				=	0,
		db_is_cache				= 	false,
		
		db_propertys			=	{},
		
			
		keyIdx = {},
		varFunc = {},
		prikeyparams = {},
		unprikeyparams = {},
		allparams = {},
		allparams_obj = {},
		prikeyparams_obj = {},
		unprikeyparams_obj = {},
	}
	setmetatable(o, self)
	self.__index = self
	return o
end

function dbclass:getParamObjs(sql_type)
	local function getUpdateWithKeyParObjList()
		local t = {}
		if self.varFunc[sql_type] and #self.varFunc[sql_type].m_paramList > 0 then
			local tempList = self.varFunc[sql_type].m_paramList		
			for k,v in pairs(self.unprikeyparams_obj) do
				for i,w in ipairs(tempList) do				
					if v.dbpro_name == w then
						table.insert(t,v)
						break
					end	
				end		
			end
		end	
		if #t == 0 then
			t = self.unprikeyparams_obj
		end
		
		return t		
	end
	
	if sql_type == "update_withkey" then
		return getUpdateWithKeyParObjList()
	end
	
	local t = {}
	if self.varFunc[sql_type] and #self.varFunc[sql_type].m_paramList > 0 then
		local tempList = self.varFunc[sql_type].m_paramList		
		for k,v in pairs(self.allparams_obj) do
			for i,w in ipairs(tempList) do				
				if v.dbpro_name == w then
					table.insert(t,v)
					break
				end	
			end
		end	
		
	end	
	if #t == 0 then
			t = self.allparams_obj
	end	
	return t
end


function dbclass:getParamList(sql_type)
	local function getUpdateWithKeyParList()
		local t = {}
		if self.varFunc[sql_type] and #self.varFunc[sql_type].m_paramList > 0 then
			local tempList = self.varFunc[sql_type].m_paramList		
			for k,v in pairs(self.unprikeyparams) do
				for i,w in ipairs(tempList) do				
					if v[1] == w then
						table.insert(t,v)
						break
					end	
				end		
			end
		end	
		if #t == 0 then
			t = self.unprikeyparams
		end
		
		return t		
	end	
	
	if sql_type == "update_withkey" then
		return getUpdateWithKeyParList()
	end
	
	local t = {}
	if self.varFunc[sql_type] and #self.varFunc[sql_type].m_paramList > 0 then
		local tempList = self.varFunc[sql_type].m_paramList		
		for k,v in pairs(self.allparams) do
			for i,w in ipairs(tempList) do				
				if v[1] == w then
					table.insert(t,v)
					break
				end	
			end		
		end	
	end

	if #t == 0 then
		t = self.allparams
	end
	
	return t
end

local sql_type,tempParList,tempParams_obj

local	dbclassMgr = {}
local maping = dbmap:find('maping')
if maping == nil then
	print("load maping node fail!")
	return
end

local function split(str, sep)
	sep = sep or ','
	local fields = {}
	local matchfunc = string.gmatch(str, "([^"..sep.."]+)")
	if not matchfunc then return {str} end
	for str in matchfunc do
		str = tonumber(str)
	    table.insert(fields, str)
	end
	return fields
end

local varFuncNameList = {
	"select_withclumn_and",
	"select_withclumn_or",
	"select_count_withand",
	"select_count_withor",
	"sel_topn_withclumn_and",
	"sel_topn_withclumn_or",
	"update_withkey",
	"select_colum_maxvalue",
}

for k , v in ipairs(maping) do
	local clsObj = dbclass:new()
	clsObj.db_name		=	v.name
	clsObj.db_table		=	v.table
	clsObj.db_vdbId		=	tonumber(v.virdb)
	clsObj.db_cacheId	=	tonumber(v.cacher)
	clsObj.db_is_cache 	= 	v.is_cache
	
	for i,v in ipairs(varFuncNameList) do
		clsObj.varFunc[v] = {
			m_paramList = {},
			m_strFuncHead = "",
			m_strFunc = "",
		}
	end	
	for kp , vp in ipairs(v) do -- class node
		local pro = dbproperty:new()
		pro.dbpro_name		=	vp.name
		pro.dbpro_type		=	vp.type
		if vp.type == "string" then
			if vp.default then
				pro.dbpro_default	=	vp.default
			end
		elseif vp.type == "blob" then
			pro.dbpro_default = nil
		else
			if vp.default then
				pro.dbpro_default	=	tonumber(vp.default)
			end
		end
		pro.dbpro_default	=	vp.default
		pro.dbpro_des		=	vp.des
		for kc , vc in ipairs(vp) do -- property node
			if vc[0] == "column" then
				local column			=	dbcolumn:new()
				column.column_name		=	vc.name or column.column_name
				if vc.length ~= nil then
					column.column_length	=	tonumber(vc.length)
				end
				column.column_notnull	=	toBoolean(vc.not_null) or column.column_notnull
				column.column_primarykey=	toBoolean(vc.primary_key) or column.column_primarykey
				column.column_index		=	toBoolean(vc.index) or column.column_index
				pro.dbpro_column		=	column
				
				if toBoolean(vc.primary_key) then -- 记录主键idx					
					table.insert(clsObj.keyIdx,(kc-1))
				end
				
				break
			end
		end
		for kch , vch in ipairs(vp) do
			if vch[0] == "cache" then
				local cache				=	dbcache:new()
				if vch.emergent_lev ~= nil then
					cache.cache_emergent	=	tonumber(vch.emergent_lev)
				end
				pro.dbpro_cache			=	cache
				break
			end
		end	
		table.insert(clsObj.db_propertys,pro)
		
		if vp.support_sql_list then
			local tyList = split(vp.support_sql_list,",")
			for i,v in ipairs(tyList) do				
				local name = varFuncNameList[v]						
				if clsObj.varFunc[name].m_paramList then					
					table.insert(clsObj.varFunc[name].m_paramList,vp.name)				
				end
			end
		end		
	end	
	
	for pk , pv in ipairs(clsObj.db_propertys) do
		if pv.dbpro_column.column_primarykey == true then
			table.insert(clsObj.prikeyparams,{pv.dbpro_name,pv.dbpro_type})
			table.insert(clsObj.prikeyparams_obj,pv)
		else
			table.insert(clsObj.unprikeyparams,{pv.dbpro_name,pv.dbpro_type})
			table.insert(clsObj.unprikeyparams_obj,pv)
		end
	end
	
	for k2 ,v2 in ipairs(clsObj.prikeyparams) do
		table.insert(clsObj.allparams,{v2[1],v2[2]})
	end
	for k2 ,v2 in ipairs(clsObj.unprikeyparams) do
		table.insert(clsObj.allparams,{v2[1],v2[2]})
	end
	for k2 ,v2 in ipairs(clsObj.prikeyparams_obj) do
		table.insert(clsObj.allparams_obj,v2)
	end
	for k2 ,v2 in ipairs(clsObj.unprikeyparams_obj) do
		table.insert(clsObj.allparams_obj,v2)
	end
	
	if #clsObj.prikeyparams == 0 then
		assert(false,clsObj.db_name.." hasn't primary_key!")
		return
	end
	
	table.insert(dbclassMgr,clsObj)
end

local	CT2SQLT	=	{
	int8_t		=	"tinyint",
	uint8_t		=	"tinyint UNSIGNED",
	int16_t		=	"smallint",
	uint16_t	=	"smallint UNSIGNED",
	int32_t		=	"int",
	uint32_t	=	"int UNSIGNED",
	string		=	"varchar",
	blob		=	"blob",
}

function getTypeStr(proper)
	if proper == nil then
		return false
	end
	local sqltype = CT2SQLT[proper.dbpro_type]
	if sqltype == nil then
		return false
	end
	if proper.dbpro_type == "string" then
		if proper.dbpro_column.column_length == nil or proper.dbpro_column.column_length == 0 then
			return false
		end
		return true , sqltype.."("..proper.dbpro_column.column_length..")"
	else
		return true , sqltype
	end
end

function getNotnullStr(proper)
	if proper == nil then
		return false , ""
	end
	if proper.dbpro_column == nil then
		return false , ""
	end
	if proper.dbpro_column.column_notnull then
		return true , "NOT NULL"
	end
	return true , ""
end

function getDefaultStr(proper)
	if proper == nil then
		return false , ""
	end
	if proper.dbpro_default == nil then
		return true , ""
	end
	if proper.dbpro_type == "string" then
		return true , "DEFAULT '"..proper.dbpro_default.."'"
	elseif proper.dbpro_type == "blob" then
		return true , ""
	else
		return true , "DEFAULT "..proper.dbpro_default
	end	
end

function getCommentStr(proper)
	if proper == nil then
		return false , ""
	end
	if proper.dbpro_des == nil then
		return false , ""
	end
	if proper.dbpro_des == "" then
		return true , ""
	end
	return true , "COMMENT '"..proper.dbpro_des.."'"
end

function getPrimarykeyStr(dbclass)
	if dbclass == nil then
		return false , ""
	end
	local prikeys = {}
	for k , v in ipairs(dbclass.db_propertys) do
		if v.dbpro_column and v.dbpro_column.column_primarykey == true then
			table.insert(prikeys,v.dbpro_column.column_name)
		end
	end
	if #prikeys == 0 then
		return true , ""
	else
		local ret = "PRIMARY KEY ("
		for k , v in ipairs(prikeys) do
			if k == 1 then
				ret = ret.."`"..v.."`"
			else
				ret = ret.." , `"..v.."`"
			end
		end
		ret = ret..")"
		return true , ret
	end
end

function getIndexStr(dbclass)
	if dbclass == nil then
		return false , ""
	end
	local indexs = {}
	for k , v in ipairs(dbclass.db_propertys) do
		if v.dbpro_column and v.dbpro_column.column_index == true then
			table.insert(indexs,v.dbpro_column.column_name)
		end
	end
	if #indexs == 0 then
		return true , ""
	else
		local ret = ""
		for k , v in ipairs(indexs) do
			if k == #indexs then
				ret = ret..[[KEY `]]..v..[[` (`]]..v..[[`)]]
			else
				ret = ret..[[KEY `]]..v..[[` (`]]..v..[[`),]]
			end
		end
		return true , ret
	end
end

--create tables sql
local crate_sql_file			=	io.open(__PATH__.."\\sql\\createTable.sql","w")
local crate_sql_str				=	""
for k , v in ipairs(dbclassMgr) do
	crate_sql_str	=	crate_sql_str..[[
DROP TABLE IF EXISTS `]]..v.db_table..[[`;
CREATE TABLE `]]..v.db_table..[[` (
	]]
	for kp , vp in ipairs(v.db_propertys) do
		local getstrret , TypeStr , NotnullStr , DefaultStr , CommentStr 
		getstrret , TypeStr 		= getTypeStr(vp)
		if not getstrret then
			assert(false,"getTypeStr fail!\n")
			return
		end
		getstrret , NotnullStr		= getNotnullStr(vp)
		if not getstrret then
			assert(false,"getNotnullStr fail!\n")
			return
		end
		getstrret , DefaultStr		= getDefaultStr(vp)
		if not getstrret then
			assert(false,"getDefaultStr fail!\n")
			return
		end
		getstrret , CommentStr		= getCommentStr(vp)
		if not getstrret then
			assert(false,"getCommentStr fail!\n")
			return
		end
		
		if vp.dbpro_column.column_name == nil then
			-- print(vp.dbpro_name)
			-- dumpTable(v)
		end
		-- print(vp.dbpro_column.column_length)
		if kp==1 then
			crate_sql_str	=	crate_sql_str.."`"..vp.dbpro_column.column_name.."` "..TypeStr.." "..NotnullStr.." "..DefaultStr.." "..CommentStr
		else
			crate_sql_str	=	crate_sql_str..[[,
	`]]..vp.dbpro_column.column_name.."` "..TypeStr.." "..NotnullStr.." "..DefaultStr.." "..CommentStr
		end	
	end
	local getstrret , PrimarykeyStr , IndexStr
	getstrret , PrimarykeyStr	= getPrimarykeyStr(v)
	if not getstrret then
		assert(false,"getPrimarykeyStr fail!\n")
		return
	end
	getstrret , IndexStr		= getIndexStr(v)
	if not getstrret then
		assert(false,"getIndexStr fail!\n")
		return
	end
	if PrimarykeyStr ~= "" then
		crate_sql_str	=	crate_sql_str..[[,
	]]..PrimarykeyStr
	end
	if IndexStr ~= "" then
		crate_sql_str	=	crate_sql_str..[[,
	]]..IndexStr
	end
	crate_sql_str	=	crate_sql_str..[[

) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

]]
end

crate_sql_file:write(crate_sql_str)
crate_sql_file:close()



--create class header
local class_header_file			=	io.open(__PATH__.."\\dbclass\\DbClass.h","w")
local class_header_str			=	[[
#ifndef __DBCLASS_H__
#define __DBCLASS_H__
#include "BaseStdInt.h"
#include "DbOpt.h"

extern std::vector<std::string> gCacheTblNames;	// global cfg var

]]


local	CT2CT	=	{
	int8_t		=	"int8_t",
	uint8_t		=	"uint8_t",
	int16_t		=	"int16_t",
	uint16_t	=	"uint16_t",
	int32_t		=	"int32_t",
	uint32_t	=	"uint32_t",
	string		=	"std::string",
	blob		=	"std::string",
}

local	CT2VART	=	{
	int8_t		=	"DBVAR_I8",
	uint8_t		=	"DBVAR_U8",
	int16_t		=	"DBVAR_I16",
	uint16_t	=	"DBVAR_U16",
	int32_t		=	"DBVAR_I32",
	uint32_t	=	"DBVAR_U32",
	string		=	"DBVAR_STR",
	blob		=	"DBVAR_BLOB",
}
	
local function toParam(t)
	local typeStr = CT2CT[t[2]]
	if typeStr == nil then
		return nil
	end
	return typeStr.." "..t[1]
end

local function toFlagParam(t)
	return "bool flag_"..t[1]..""
end

local function toFlagParamAndDefault(t,flag)
	if flag then
		return "bool flag_"..t[1].." = true"
	else
		return "bool flag_"..t[1].." = false"
	end
end


local function toParamList(list)
	local retStr = ""
	for k , v in ipairs(list) do
		local ret = toParam(v)
		if ret == nil then
			return nil
		end
		if k == 1 then
			retStr = retStr..ret
		else
			retStr = retStr.." , "..ret
		end
	end
	return retStr
end

local function toParamPtr(t)
	local typeStr = CT2CT[t[2]]
	if typeStr == nil then
		return nil
	end
	return typeStr.."* "..t[1]
end

local function toParamPtrDefault(t)
	local typeStr = CT2CT[t[2]]
	if typeStr == nil then
		return nil
	end
	return typeStr.."* "..t[1].." = NULL"
end

local function toParamPtrList(t)
	local retStr = ""
	for k , v in ipairs(t) do
		local ret = toParamPtr(v)
		if ret == nil then
			return nil
		end
		if k == 1 then
			retStr = retStr..ret
		else
			retStr = retStr.." , "..ret
		end
	end
	return retStr
end

local function toParamPtrListDefault(t)
	local retStr = ""
	for k , v in ipairs(t) do
		local ret = toParamPtrDefault(v)
		if ret == nil then
			return nil
		end
		if k == 1 then
			retStr = retStr..ret
		else
			retStr = retStr.." , "..ret
		end
	end
	return retStr
end

local function toParamFlagList(t)
	local retStr = ""
	for k , v in ipairs(t) do
		local ret = toFlagParam(v)
		if ret == nil then
			return nil
		end
		if k == 1 then
			retStr = retStr..ret
		else
			retStr = retStr.." , "..ret
		end
	end
	return retStr
end

local function toParamFlagListAndDefault(t,flag)
	local retStr = ""
	for k , v in ipairs(t) do
		local ret = toFlagParamAndDefault(v,flag)
		if ret == nil then
			return nil
		end
		if k == 1 then
			retStr = retStr..ret
		else
			retStr = retStr.." , "..ret
		end
	end
	return retStr
end


local function toCacheKeyList(t)
	local retStr = ""
	for k , v in ipairs(t) do
		retStr = retStr..[[<<":"<<]]..v[1]
	end	
	return string.sub(retStr,6) --skip first "<<:"
end

for k , v in ipairs(dbclassMgr) do	
	local insertStr = ""
	local repalceStr = ""
	local updateStr = ""
	local deleteStr = ""
	local selectStr = ""
	local selectStr2 = ""
	local selectStr3 = ""
	local selectCountStr = ""
	local selectCountWithOrStr = ""
	local selectMaxStr = ""
	local exeCmdStr = ""
	local selectStr4 = ""
	local selectStr5 = ""
	if #v.allparams > 0 then
		local tempParamListStr = toParamList(v.allparams)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		insertStr = [[static CDBBaseOpt*	insert(]]..tempParamListStr..[[);]].."\n"
		repalceStr = [[static CDBBaseOpt*	replaceInto(]]..tempParamListStr..[[);
]]
	else
		insertStr = [[static CDBBaseOpt*	insert();]]
		repalceStr = [[static CDBBaseOpt*	insert();
]]
	end
	
	sql_type = "update_withkey"
	tempParList = v:getParamList(sql_type)	
	updateStr = [[static CDBBaseOpt*	update_withkey(]]
	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert("toParamList = nil")
			return
		end
		updateStr = updateStr..tempParamListStr
	end
	
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		updateStr = updateStr..[[ , ]]..tempParamListStr
	end
	updateStr = updateStr..[[);
]]

	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert("toParamList = nil")
			return
		end
		deleteStr = [[static CDBBaseOpt*	delete_withkey( ]]..tempParamListStr..[[);
]]
	end
	
	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert("toParamList = nil")
			return
		end
		selectStr = [[static CDBBaseOpt*	select_withkey( ]]..tempParamListStr
	end
	-- if #v.unprikeyparams > 0 then
		-- local tempParamListStr = toParamFlagListAndDefault(v.unprikeyparams,true)
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagListAndDefault(v.allparams,true)
		if tempParamListStr == nil then
			assert("toParamFlagListAndDefault = nil")
			return
		end
		selectStr = selectStr..[[ , ]]..tempParamListStr
	end
	selectStr = selectStr..[[);
]]
	
	sql_type = "select_withclumn_and"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectStr2 = [[static CDBBaseOpt*	select_withclumn_and( ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagListAndDefault(v.allparams,true)
		if tempParamListStr == nil then
			assert("toParamFlagListAndDefault = nil")
			return
		end
		selectStr2 = selectStr2..[[ , ]]..tempParamListStr
	end
	selectStr2 = selectStr2..[[);
]]

	sql_type = "select_withclumn_or"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectStr3 = [[static CDBBaseOpt*	select_withclumn_or( ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagListAndDefault(v.allparams,true)
		if tempParamListStr == nil then
			assert("toParamFlagListAndDefault = nil")
			return
		end
		selectStr3 = selectStr3..[[ , ]]..tempParamListStr
	end
	selectStr3 = selectStr3..[[);
]]

	sql_type = "select_count_withand"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectCountStr = [[static CDBBaseOpt*	select_count_withand( ]]..tempParamListStr..[[);
]]
	end

	sql_type = "select_count_withor"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectCountWithOrStr = [[static CDBBaseOpt*	select_count_withor( ]]..tempParamListStr..[[);
]]
	end
	
	sql_type = "select_colum_maxvalue"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

	if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectMaxStr = [[static CDBBaseOpt*	select_colum_maxvalue( std::string columName , ]]..tempParamListStr..[[);
]]
	end

	exeCmdStr = [[static CDBBaseOpt*	exe_cmd( int8_t blob , int32_t resultType , const std::string& sqlStr , const std::string& cacheId , const std::string& cacheParam , const std::vector<CDBFieldDef>& columnDefs);
]]

-- select top n withCloumnAnd
	sql_type = "sel_topn_withclumn_and"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectStr4 = [[static CDBBaseOpt*	sel_topn_withclumn_and(uint32_t selLimitCnt, ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagListAndDefault(v.allparams,true)
		if tempParamListStr == nil then
			assert("toParamFlagListAndDefault = nil")
			return
		end
		selectStr4 = selectStr4..[[ , ]]..tempParamListStr
	end
	selectStr4 = selectStr4..[[);
]]

-- select top n withCloumnOr
	sql_type = "sel_topn_withclumn_or"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

if #tempParList > 0 then
		local tempParamListStr = toParamPtrListDefault(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrListDefault = nil")
			return
		end
		selectStr5 = [[static CDBBaseOpt*	sel_topn_withclumn_or(uint32_t selLimitCnt, ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagListAndDefault(v.allparams,true)
		if tempParamListStr == nil then
			assert("toParamFlagListAndDefault = nil")
			return
		end
		selectStr5 = selectStr5..[[ , ]]..tempParamListStr
	end
	selectStr5 = selectStr5..[[);
]]
	
	
class_header_str			=	class_header_str..[[
class	]]..v.db_name..[[{
public:
	]]..v.db_name..[[(){}
	~]]..v.db_name..[[(){}
	static uint32_t	GetVirDbId(){ return ]]..v.db_vdbId..[[;}
	static void Init();
	static const std::vector<CDBFieldDef>& GetFieldDefs();
	static uint16_t Name2Idx(const std::string& def);
	static const std::string& Idx2Name(uint16_t idx);
	]]..insertStr..[[
	]]..selectStr..[[
	]]..updateStr
	
if v.db_is_cache == "false" then
	class_header_str = class_header_str..[[
	]]..repalceStr..[[
	]]..deleteStr..[[
	]]..selectStr2..[[
	]]..selectStr3..[[
	]]..selectCountStr..[[
	]]..selectCountWithOrStr..[[
	]]..selectMaxStr..[[
	]]..exeCmdStr..[[
	]]..selectStr4..[[
	]]..selectStr5
end
	
class_header_str = class_header_str..[[	
public:
	static std::string m_tableName;
	static std::vector<CDBFieldDef> m_vectColumnDefs;
	static std::map<std::string,uint16_t> m_name2Idx;
	static bool m_bIsCacheTbl;
	static std::pair<int16_t, int16_t> m_pairKeyIdx; // index start from 0

};
]]
end

class_header_str = class_header_str ..[[void GlobalTblClsInit();]].."\n"
class_header_str = class_header_str ..[[const std::string& Idx2Name(const std::string& tblName,uint16_t idx);]].."\n"
class_header_str = class_header_str ..[[uint16_t Name2Idx(const std::string& tblName, const std::string& name);]].."\n"
class_header_str = class_header_str ..[[#endif //__DBCLASS_H__]].."\n"
class_header_file:write(class_header_str)
class_header_file:close()

--create class cppfile
local class_cpp_file			=	io.open(__PATH__.."\\dbclass\\DbClass.cpp","w")
local class_cpp_str				=	[[
#include<sstream>
#include<iostream>
#include<assert.h>
#include "DbClass.h"

std::vector<std::string> gCacheTblNames;

]]

for k , v in ipairs(dbclassMgr) do	
	varDefineStr = [[std::string ]]..v.db_name..[[::m_tableName = "]] .. v.db_table..[[";]]
	varDefineStr = varDefineStr .. "\n"..[[std::vector<CDBFieldDef> ]]..v.db_name.. [[::m_vectColumnDefs;]]
	varDefineStr = varDefineStr .. "\n".. [[std::map<std::string,uint16_t> ]]..v.db_name..[[::m_name2Idx;]]
	varDefineStr = varDefineStr .. "\n".. [[bool ]]..v.db_name..[[::m_bIsCacheTbl = false;]].."\n"
	v.keyIdx[2] = v.keyIdx[2] or -1
	varDefineStr = varDefineStr .. [[std::pair<int16_t, int16_t> ]]..v.db_name..[[::m_pairKeyIdx = std::make_pair(]]..v.keyIdx[1]..","..v.keyIdx[2]..[[);]].."\n\n"
	insertStr = ""
	repalceStr = ""
	updateStr = ""
	deleteStr = ""
	selectStr = ""
	selectStr2 = ""
	selectStr3 = ""
	selectStr4 = ""
	selectStr5 = ""
	clsInitStr	= ""
	getFieldStr = ""
	Name2IdxStr = ""
	Idx2NameStr = ""
	selectCountStr = ""
	selectCountWithOrStr = ""
	selectMaxStr = ""
	
------------------------------------------insert
	if #v.allparams > 0 then
		local tempParamListStr = toParamList(v.allparams)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		insertStr = [[CDBBaseOpt*	]]..v.db_name..[[::insert( ]]..tempParamListStr..[[)
]]
	else
		insertStr = [[CDBBaseOpt*	]]..v.db_name..[[::insert()
]]
	end
	insertStr	=	insertStr..[[
{
	CDBOpt_Insert* pOpt = static_cast<CDBOpt_Insert*>(CDBBaseOpt::CreateOptObj(OPTTYPE_INSERT));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey]]..toCacheKeyList(v.prikeyparams)..[[;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "]]..v.db_table..[[:" +  pOpt->m_cacheKey;
]]
	
	for ak , av in ipairs(v.allparams_obj) do
		insertStr = insertStr..[[	
	CDBFieldInfo	]]..av.dbpro_name..[[_info;
	]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
	]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
	]]
		if av.dbpro_type == "blob" then
			insertStr = insertStr..av.dbpro_name..[[_info.m_fieldValue.SetBlob(]]..av.dbpro_name..[[);
	]]
		else
			insertStr = insertStr..av.dbpro_name..[[_info.m_fieldValue.SetValue(]]..av.dbpro_name..[[);
	]]
		end
		insertStr = insertStr..[[pOpt->m_fieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	end
	insertStr = insertStr..[[
	return pOpt;
}
]]

------------------------------------------replace into
	if #v.allparams > 0 then
		local tempParamListStr = toParamList(v.allparams)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		repalceStr = [[CDBBaseOpt*	]]..v.db_name..[[::replaceInto( ]]..tempParamListStr..[[)
]]
	else
		repalceStr = [[CDBBaseOpt*	]]..v.db_name..[[::replaceInto()
]]
	end
	repalceStr	=	repalceStr..[[
{
	CDBOpt_ReplaceInto* pOpt = static_cast<CDBOpt_ReplaceInto*>(CDBBaseOpt::CreateOptObj(OPTTYPE_REPLACEINTO));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey]]..toCacheKeyList(v.prikeyparams)..[[;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "]]..v.db_table..[[:" +  pOpt->m_cacheKey;
]]
	
	for ak , av in ipairs(v.allparams_obj) do
		repalceStr = repalceStr..[[	
	CDBFieldInfo	]]..av.dbpro_name..[[_info;
	]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
	]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
	]]
		if av.dbpro_type == "blob" then
			repalceStr = repalceStr..av.dbpro_name..[[_info.m_fieldValue.SetBlob(]]..av.dbpro_name..[[);
	]]
		else
			repalceStr = repalceStr..av.dbpro_name..[[_info.m_fieldValue.SetValue(]]..av.dbpro_name..[[);
	]]
		end
		repalceStr = repalceStr..[[pOpt->m_fieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	end
	repalceStr = repalceStr..[[
	return pOpt;
}
]]

--------------------------------------update_withkey
	sql_type = "update_withkey"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	updateStr = [[CDBBaseOpt*	]]..v.db_name..[[::update_withkey(]]
	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert(false,"toParamList = nil")
			return
		end
		updateStr = updateStr..tempParamListStr
	end	
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		updateStr = updateStr..[[ , ]]..tempParamListStr
	end
	updateStr = updateStr..[[)
{
	]]

	updateStr = updateStr..[[if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			updateStr = updateStr..[[!]]..upv.dbpro_name
		else
			updateStr = updateStr..[[ &&!]]..upv.dbpro_name
		end
	end
	updateStr = updateStr..[[)
		return nullptr;
	CDBOpt_Update_WithKey* pOpt = static_cast<CDBOpt_Update_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_UPDATE_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey]]..toCacheKeyList(v.prikeyparams)..[[;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "]]..v.db_table..[[:" +  pOpt->m_cacheKey;
]]
	for ak , av in ipairs(v.prikeyparams_obj) do
		updateStr = updateStr..[[	
	CDBFieldInfo	]]..av.dbpro_name..[[_info;
]].."	"..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
]].."	"..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			updateStr = updateStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(]]..av.dbpro_name..[[);
]]
		else
			updateStr = updateStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetValue(]]..av.dbpro_name..[[);
]]
		end
		updateStr = updateStr.."	"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	end
	
	for ak , av in ipairs(tempParams_obj) do
		updateStr = updateStr..[[
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
		]]
		if av.dbpro_type == "blob" then
			updateStr = updateStr..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
		]]
		else
			updateStr = updateStr..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
		]]
		end
		updateStr = updateStr..[[pOpt->m_valueFieldInfos.push_back(]]..av.dbpro_name..[[_info);
	}
]]
	end
	updateStr = updateStr..[[
	return pOpt;
}
]]

---------------------------------delete_withkey
	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert(false,"toParamList = nil")
			return
		end
		deleteStr = [[CDBBaseOpt*	]]..v.db_name..[[::delete_withkey( ]]..tempParamListStr..[[)
{
]]
	end
	
	deleteStr = deleteStr..[[
	CDBOpt_Del_WithKey* pOpt = static_cast<CDBOpt_Del_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_DELETE_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey]]..toCacheKeyList(v.prikeyparams)..[[;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "]]..v.db_table..[[:" +  pOpt->m_cacheKey;
]]
	for ak , av in ipairs(v.prikeyparams_obj) do
		deleteStr = deleteStr..[[	
	CDBFieldInfo	]]..av.dbpro_name..[[_info;
	]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
	]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			deleteStr = deleteStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(]]..av.dbpro_name..[[);
]]
		else
			deleteStr = deleteStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetValue(]]..av.dbpro_name..[[);
]]
		end
		deleteStr = deleteStr.."	"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	end

	deleteStr = deleteStr..[[
	return pOpt;
}
]]
	
	
-------------------------------select_withkey
	if #v.prikeyparams > 0 then
		local tempParamListStr = toParamList(v.prikeyparams)
		if tempParamListStr == nil then
			assert(false,"toParamList = nil")
			return
		end
		selectStr = [[CDBBaseOpt*	]]..v.db_name..[[::select_withkey( ]]..tempParamListStr
	end
	-- if #v.unprikeyparams > 0 then
		-- local tempParamListStr = toParamFlagList(v.unprikeyparams)
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagList(v.allparams)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		selectStr = selectStr..[[ , ]]..tempParamListStr
	end
	selectStr = selectStr..[[)
{
]]
	selectStr = selectStr..[[	if(]]
	-- for upk , upv in ipairs(v.unprikeyparams_obj) do
	for upk , upv in ipairs(v.allparams_obj) do
		if upk == 1 then
			selectStr = selectStr..[[!flag_]]..upv.dbpro_name
		else
			selectStr = selectStr..[[ &&!flag_]]..upv.dbpro_name
		end
	end
	selectStr = selectStr..[[)
		return nullptr;
	CDBOpt_Sel_WithKey* pOpt = static_cast<CDBOpt_Sel_WithKey*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHKEY));
	pOpt->m_tableName	=	m_tableName;
	std::stringstream ss_cacheKey;
	ss_cacheKey]]..toCacheKeyList(v.prikeyparams)..[[;
	pOpt->m_cacheKey	=	ss_cacheKey.str();
	pOpt->m_primaryKey 	=  "]]..v.db_table..[[:" +  pOpt->m_cacheKey;
]]
	for ak , av in ipairs(v.prikeyparams_obj) do
		selectStr = selectStr..[[	
	CDBFieldInfo	]]..av.dbpro_name..[[_info;
	]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
	]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectStr = selectStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(]]..av.dbpro_name..[[);
]]
		else
			selectStr = selectStr.."	"..av.dbpro_name..[[_info.m_fieldValue.SetValue(]]..av.dbpro_name..[[);
]]
		end
		selectStr = selectStr.."	"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	end
	
	-- for ak , av in ipairs(v.unprikeyparams_obj) do
	for ak , av in ipairs(v.allparams_obj) do	
		selectStr = selectStr..[[
	if(flag_]]..av.dbpro_name..[[)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		fieldDef.m_fieldType	=	]]..CT2VART[av.dbpro_type]..[[;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
]]
	end
	selectStr = selectStr..[[
	return pOpt;
}
]]


-------------------------------select_withclumn_and
	sql_type = "select_withclumn_and"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr2 = [[CDBBaseOpt*	]]..v.db_name..[[::select_withclumn_and( ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagList(v.allparams)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr2 = selectStr2..[[ , ]]..tempParamListStr
	end
	selectStr2 = selectStr2..[[)
{
]]
	selectStr2 = selectStr2..[[		
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectStr2 = selectStr2..[[!]]..upv.dbpro_name
		else
			selectStr2 = selectStr2..[[ &&!]]..upv.dbpro_name
		end
	end
	selectStr2 = selectStr2..[[)
		return nullptr;
]]
	selectStr2 = selectStr2..[[	if(]]
	for upk , upv in ipairs(v.allparams_obj) do
		if upk == 1 then
			selectStr2 = selectStr2..[[!flag_]]..upv.dbpro_name
		else
			selectStr2 = selectStr2..[[ &&!flag_]]..upv.dbpro_name
		end
	end
	selectStr2 = selectStr2..[[)
		return nullptr;
	CDBOpt_Sel_WithClumnAnd* pOpt = static_cast<CDBOpt_Sel_WithClumnAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHCLUMNAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
]]

	for ak , av in ipairs(tempParams_obj) do
		selectStr2 = selectStr2..[[
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectStr2 = selectStr2.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectStr2 = selectStr2.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectStr2 = selectStr2.."		"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	selectStr2 = selectStr2..[[
	}
]]
	end
	
	for ak , av in ipairs(v.allparams_obj) do
		selectStr2 = selectStr2..[[
	if(flag_]]..av.dbpro_name..[[)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		fieldDef.m_fieldType	=	]]..CT2VART[av.dbpro_type]..[[;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
]]
	end
	selectStr2 = selectStr2..[[
	return pOpt;
}
]]

clsInitStr = [[void	]]..v.db_name..[[::Init()
{
	if(m_vectColumnDefs.empty())
	{
]]

	for getPk , getPv in ipairs(v.db_propertys) do
	clsInitStr = clsInitStr..[[
		CDBFieldDef	_]]..getPk..[[_field;	
		_]]..getPk..[[_field.Init("]]..getPv.dbpro_column.column_name..[[",]]..CT2VART[getPv.dbpro_type]..[[,(uint16_t)m_vectColumnDefs.size());		
		m_vectColumnDefs.push_back(_]]..getPk..[[_field);]].."\n"
	end 
	clsInitStr = clsInitStr..[[	
		for(size_t i = 0; i < m_vectColumnDefs.size();++i)
		{
			m_name2Idx[m_vectColumnDefs[i].m_fieldName] = (uint16_t)i;
		}
		
		m_bIsCacheTbl = ]] .. v.db_is_cache ..[[;
	}
}	
]]


-------------------------------sel_topn_withclumn_and
	sql_type = "sel_topn_withclumn_and"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr4 = [[CDBBaseOpt*	]]..v.db_name..[[::sel_topn_withclumn_and(uint32_t selLimitCnt, ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagList(v.allparams)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr4 = selectStr4..[[ , ]]..tempParamListStr
	end
	selectStr4 = selectStr4..[[)
{
]]
	selectStr4 = selectStr4..[[		
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectStr4 = selectStr4..[[!]]..upv.dbpro_name
		else
			selectStr4 = selectStr4..[[ &&!]]..upv.dbpro_name
		end
	end
	selectStr4 = selectStr4..[[)
		return nullptr;
]]
	selectStr4 = selectStr4..[[	if(]]
	for upk , upv in ipairs(v.allparams_obj) do
		if upk == 1 then
			selectStr4 = selectStr4..[[!flag_]]..upv.dbpro_name
		else
			selectStr4 = selectStr4..[[ &&!flag_]]..upv.dbpro_name
		end
	end
	selectStr4 = selectStr4..[[)
		return nullptr;
	CDBOpt_SelTopN_WithClumnAnd* pOpt = static_cast<CDBOpt_SelTopN_WithClumnAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECTTOPN_WITHCLUMNAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	pOpt->m_nSelLimitCnt	= selLimitCnt;
]]

	for ak , av in ipairs(tempParams_obj) do
		selectStr4 = selectStr4..[[
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectStr4 = selectStr4.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectStr4 = selectStr4.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectStr4 = selectStr4.."		"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	selectStr4 = selectStr4..[[
	}
]]
	end
	
	for ak , av in ipairs(v.allparams_obj) do
		selectStr4 = selectStr4..[[
	if(flag_]]..av.dbpro_name..[[)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		fieldDef.m_fieldType	=	]]..CT2VART[av.dbpro_type]..[[;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
]]
	end
	selectStr4 = selectStr4..[[
	return pOpt;
}
]]

-------------------------------sel_topn_withclumn_or
	sql_type = "sel_topn_withclumn_or"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr5 = [[CDBBaseOpt*	]]..v.db_name..[[::sel_topn_withclumn_or(uint32_t selLimitCnt, ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagList(v.allparams)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr5 = selectStr5..[[ , ]]..tempParamListStr
	end
	selectStr5 = selectStr5..[[)
{
]]
	selectStr5 = selectStr5..[[		
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectStr5 = selectStr5..[[!]]..upv.dbpro_name
		else
			selectStr5 = selectStr5..[[ &&!]]..upv.dbpro_name
		end
	end
	selectStr5 = selectStr5..[[)
		return nullptr;
]]
	selectStr5 = selectStr5..[[	if(]]
	for upk , upv in ipairs(v.allparams_obj) do
		if upk == 1 then
			selectStr5 = selectStr5..[[!flag_]]..upv.dbpro_name
		else
			selectStr5 = selectStr5..[[ &&!flag_]]..upv.dbpro_name
		end
	end
	selectStr5 = selectStr5..[[)
		return nullptr;
	CDBOpt_SelTopN_WithClumnOr* pOpt = static_cast<CDBOpt_SelTopN_WithClumnOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECTTOPN_WITHCLUMNOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
	pOpt->m_nSelLimitCnt	= selLimitCnt;
]]

	for ak , av in ipairs(tempParams_obj) do
		selectStr5 = selectStr5..[[
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectStr5 = selectStr5.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectStr5 = selectStr5.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectStr5 = selectStr5.."		"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	selectStr5 = selectStr5..[[
	}
]]
	end
	
	for ak , av in ipairs(v.allparams_obj) do
		selectStr5 = selectStr5..[[
	if(flag_]]..av.dbpro_name..[[)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		fieldDef.m_fieldType	=	]]..CT2VART[av.dbpro_type]..[[;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
]]
	end
	selectStr5 = selectStr5..[[
	return pOpt;
}
]]

clsInitStr = [[void	]]..v.db_name..[[::Init()
{
	if(m_vectColumnDefs.empty())
	{
]]

	for getPk , getPv in ipairs(v.db_propertys) do
	clsInitStr = clsInitStr..[[
		CDBFieldDef	_]]..getPk..[[_field;	
		_]]..getPk..[[_field.Init("]]..getPv.dbpro_column.column_name..[[",]]..CT2VART[getPv.dbpro_type]..[[,(uint16_t)m_vectColumnDefs.size());		
		m_vectColumnDefs.push_back(_]]..getPk..[[_field);]].."\n"
	end 
	clsInitStr = clsInitStr..[[	
		for(size_t i = 0; i < m_vectColumnDefs.size();++i)
		{
			m_name2Idx[m_vectColumnDefs[i].m_fieldName] = (uint16_t)i;
		}
		
		m_bIsCacheTbl = ]] .. v.db_is_cache ..[[;
	}
}	
]]

-------------------------------getFileStr
getFieldStr = [[const std::vector<CDBFieldDef>&	]]..v.db_name..[[::GetFieldDefs()
{	
	return m_vectColumnDefs;
}	
]]	



-------------------------------Name2IdxStr
Name2IdxStr = [[uint16_t	]]..v.db_name..[[::Name2Idx(const std::string& def)
{	
	return m_name2Idx[def];	
}	
]]	


-------------------------------Idx2NameStr
Idx2NameStr = [[const std::string& ]]..v.db_name..[[:: Idx2Name(uint16_t idx)
{		
	return m_vectColumnDefs[idx].m_fieldName;
}	
]]	

-------------------------------select_withclumn_or
	sql_type = "select_withclumn_or"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)

	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr3 = [[CDBBaseOpt*	]]..v.db_name..[[::select_withclumn_or( ]]..tempParamListStr..[[
]]
	end
	if #v.allparams > 0 then
		local tempParamListStr = toParamFlagList(v.allparams)
		if tempParamListStr == nil then
			assert("toParamPtrList = nil")
			return
		end
		selectStr3 = selectStr3..[[ , ]]..tempParamListStr
	end
	selectStr3 = selectStr3..[[)
{
]]
	selectStr3 = selectStr3..[[
	
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectStr3 = selectStr3..[[!]]..upv.dbpro_name
		else
			selectStr3 = selectStr3..[[ &&!]]..upv.dbpro_name
		end
	end
	selectStr3 = selectStr3..[[)
		return nullptr;
]]
	selectStr3 = selectStr3..[[	if(]]
	for upk , upv in ipairs(v.allparams_obj) do
		if upk == 1 then
			selectStr3 = selectStr3..[[!flag_]]..upv.dbpro_name
		else
			selectStr3 = selectStr3..[[ &&!flag_]]..upv.dbpro_name
		end
	end
	selectStr3 = selectStr3..[[)
		return nullptr;
	CDBOpt_Sel_WithClumnOr* pOpt = static_cast<CDBOpt_Sel_WithClumnOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_WITHCLUMNOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=	""; //不支持缓存
]]

	for ak , av in ipairs(tempParams_obj) do
		selectStr3 = selectStr3..[[
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectStr3 = selectStr3.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectStr3 = selectStr3.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectStr3 = selectStr3.."		"..[[pOpt->m_keyFieldInfos.push_back(]]..av.dbpro_name..[[_info);
]]
	selectStr3 = selectStr3..[[
	}
]]
	end
	
	for ak , av in ipairs(v.allparams_obj) do
		selectStr3 = selectStr3..[[
	if(flag_]]..av.dbpro_name..[[)
	{
		CDBFieldDef	fieldDef;
		fieldDef.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		fieldDef.m_fieldType	=	]]..CT2VART[av.dbpro_type]..[[;
		fieldDef.m_nameIdx		= 	m_name2Idx[fieldDef.m_fieldName];
		pOpt->m_columnDefs.push_back(fieldDef);
	}
]]
	end
	selectStr3 = selectStr3..[[
	return pOpt;
}
]]

---------------------------------------select_count_withand
	sql_type = "select_count_withand"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		selectCountStr = [[CDBBaseOpt*	]]..v.db_name..[[::select_count_withand( ]]..tempParamListStr..[[)
{
]]
	end
	
selectCountStr = selectCountStr..[[		
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectCountStr = selectCountStr..[[!]]..upv.dbpro_name
		else
			selectCountStr = selectCountStr..[[ &&!]]..upv.dbpro_name
		end
	end
	selectCountStr = selectCountStr..[[)
		return nullptr;
]]
	
	selectCountStr = selectCountStr..[[
	
	CDBOpt_Sel_Count_WithAnd* pOpt = static_cast<CDBOpt_Sel_Count_WithAnd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_COUNT_WITHAND));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
]]

	for ak , av in ipairs(tempParams_obj) do
		selectCountStr = selectCountStr..[[	
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectCountStr = selectCountStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectCountStr = selectCountStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectCountStr = selectCountStr.."		"..[[pOpt->m_valueFieldInfos.push_back(]]..av.dbpro_name..[[_info);
	}]]
	end
	selectCountStr = selectCountStr..[[
	
	return pOpt;
}
]]

	
	
---------------------------------------select_count_withor
	sql_type = "select_count_withor"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		selectCountWithOrStr = [[CDBBaseOpt*	]]..v.db_name..[[::select_count_withor( ]]..tempParamListStr..[[)
{
]]
	end
	
selectCountWithOrStr = selectCountWithOrStr..[[		
	if(]]
	for upk , upv in ipairs(tempParams_obj) do
		if upk == 1 then
			selectCountWithOrStr = selectCountWithOrStr..[[!]]..upv.dbpro_name
		else
			selectCountWithOrStr = selectCountWithOrStr..[[ &&!]]..upv.dbpro_name
		end
	end
	selectCountWithOrStr = selectCountWithOrStr..[[)
		return nullptr;
]]
	selectCountWithOrStr = selectCountWithOrStr..[[
	

	CDBOpt_Sel_Count_WithOr* pOpt = static_cast<CDBOpt_Sel_Count_WithOr*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_COUNT_WITHOR));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
]]

	for ak , av in ipairs(tempParams_obj) do
		selectCountWithOrStr = selectCountWithOrStr..[[	
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectCountWithOrStr = selectCountWithOrStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectCountWithOrStr = selectCountWithOrStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectCountWithOrStr = selectCountWithOrStr.."		"..[[pOpt->m_valueFieldInfos.push_back(]]..av.dbpro_name..[[_info);
	}]]
	end
	selectCountWithOrStr = selectCountWithOrStr..[[
	
	return pOpt;
}
]]


----------------------------select_colum_maxvalue
	sql_type = "select_colum_maxvalue"
	tempParList = v:getParamList(sql_type)
	tempParams_obj = v:getParamObjs(sql_type)
	
	if #tempParList > 0 then
		local tempParamListStr = toParamPtrList(tempParList)
		if tempParamListStr == nil then
			assert(false,"toParamPtrList = nil")
			return
		end
		selectMaxStr = [[CDBBaseOpt*	]]..v.db_name..[[::select_colum_maxvalue( std::string columName , ]]..tempParamListStr..[[)
{
]]
	end
	selectMaxStr = selectMaxStr..[[
	

	CDBOpt_Sel_MaxValue* pOpt = static_cast<CDBOpt_Sel_MaxValue*>(CDBBaseOpt::CreateOptObj(OPTTYPE_SELECT_MAXVALUE));
	pOpt->m_tableName	=	m_tableName;
	pOpt->m_columnName	=	columName;
	pOpt->m_cacheKey	=  ""; //不支持缓存	
]]

	for ak , av in ipairs(tempParams_obj) do
		selectMaxStr = selectMaxStr..[[	
	if(]]..av.dbpro_name..[[)
	{
		CDBFieldInfo	]]..av.dbpro_name..[[_info;
		]]..av.dbpro_name..[[_info.m_fieldName	=	"]]..av.dbpro_column.column_name..[[";
		]]..av.dbpro_name..[[_info.m_nameIdx	=	m_name2Idx[]]..av.dbpro_name..[[_info.m_fieldName];
]]
		if av.dbpro_type == "blob" then
			selectMaxStr = selectMaxStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetBlob(*]]..av.dbpro_name..[[);
]]
		else
			selectMaxStr = selectMaxStr.."		"..av.dbpro_name..[[_info.m_fieldValue.SetValue(*]]..av.dbpro_name..[[);
]]
		end
		selectMaxStr = selectMaxStr.."		"..[[pOpt->m_valueFieldInfos.push_back(]]..av.dbpro_name..[[_info);
	}]]
	end
	selectMaxStr = selectMaxStr..[[

	return pOpt;
}
]]

exeCmdStr = [[CDBBaseOpt*	]]..v.db_name..[[::exe_cmd( int8_t blob , int32_t resultType , const std::string& sqlStr , const std::string& cacheId , const std::string& cacheParam , const std::vector<CDBFieldDef>& columnDefs)
{
	
	CDBOpt_Exe_Cmd* pOpt = static_cast<CDBOpt_Exe_Cmd*>(CDBBaseOpt::CreateOptObj(OPTTYPE_EXE_CMD));
	pOpt->m_tableName = m_tableName;
	pOpt->m_resultType	=	resultType;
	pOpt->m_blob			=	blob;
	pOpt->m_sqlStr		=	sqlStr;
	pOpt->m_cacheId		=	cacheId;
	pOpt->m_cacheParam	=	cacheParam;
	pOpt->m_columnDefs.assign(columnDefs.begin(), columnDefs.end());
	for (auto iter = pOpt->m_columnDefs.begin(); iter != pOpt->m_columnDefs.end(); ++iter)
	{
		iter->m_nameIdx = m_name2Idx[iter->m_fieldName];
	}
	return pOpt;
}
]]


class_cpp_str			=	class_cpp_str..[[
]].."\n"..varDefineStr..[[
]]..clsInitStr..[[
]]..getFieldStr..[[
]]..Name2IdxStr..[[
]]..Idx2NameStr..[[
]]..insertStr..[[
]]..selectStr..[[
]]..updateStr

if v.db_is_cache == "false" then

class_cpp_str = class_cpp_str .. [[
]]..repalceStr..[[

]]..deleteStr..[[

]]..selectStr2..[[

]]..selectStr3..[[

]]..selectCountStr..[[

]]..selectCountWithOrStr..[[

]]..selectMaxStr..[[

]]..exeCmdStr..[[

]]..selectStr4..[[

]]..selectStr5..[[
]]

end

end


class_cpp_str = class_cpp_str .. [[
void GlobalTblClsInit()
{
]]
	for k , v in ipairs(dbclassMgr) do
	class_cpp_str = class_cpp_str .. [[
	]]..v.db_name..[[::Init();]].."\n"	
	end	

	for k , v in ipairs(dbclassMgr) do
	class_cpp_str = class_cpp_str .. [[
	if(]]..v.db_name..[[::m_bIsCacheTbl)
	{
		gCacheTblNames.push_back(]]..v.db_name..[[::m_tableName);
	}
	]].."\n"	
	end	
class_cpp_str = class_cpp_str ..[[	
}
]]	


class_cpp_str = class_cpp_str .. [[
const std::string& Idx2Name(const std::string& tblName,uint16_t idx)
{
	const char* strTblName = tblName.c_str();
]]
	for k , v in ipairs(dbclassMgr) do
	class_cpp_str = class_cpp_str .. [[
	if(strcmp(strTblName,"]]..v.db_table..[[") == 0)
	{
		return ]]..v.db_name..[[::Idx2Name(idx);				
	}]].."\n"
	end	
class_cpp_str = class_cpp_str..[[
	static std::string str = "";
	return str;
}
]]

class_cpp_str = class_cpp_str .. [[
uint16_t Name2Idx(const std::string& tblName, const std::string& name)
{
	const char* strTblName = tblName.c_str();
]]
	for k , v in ipairs(dbclassMgr) do
	class_cpp_str = class_cpp_str .. [[
	if(strcmp(strTblName,"]]..v.db_table..[[") == 0)
	{
		return ]]..v.db_name..[[::Name2Idx(name);				
	}]].."\n"
	end	
class_cpp_str = class_cpp_str..[[
	return 0;
}
]]	

class_cpp_file:write(class_cpp_str)
class_cpp_file:close()