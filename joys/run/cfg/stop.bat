@echo off

set program="shell.exe"

for %%i in (%program%) do (
	echo "Now killing %%i"
	taskkill /f /im %%i
)

@echo on