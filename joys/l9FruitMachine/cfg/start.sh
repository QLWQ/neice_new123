#!/bin/sh
#
# name     : tmuxenï¼ tmux environment made easy
# author   : Biyuan Li bill.allen@163.com
# license  : QKA
# created  : 2017 May 02
# modified : 2017 may 02
#


ulimit -c unlimited
ulimit -n 600000

../bin/shell ./l9ServerCfg.xml scene_fm1 ../bin/libl9FruitMachine & > /dev/null 2>&1

exit 0

