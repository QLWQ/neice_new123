#!/bin/bash
#本脚本只是针对单个进程启动使用，适用场景是运维的监控系统自动调用
chmod 777 *

ulimit -c unlimited
ulimit -n 60000

curpath=$PWD
binPath=../bin

chmod +x $binPath/shell

$binPath/shell l9ServerCfg.xml $1 $binPath/libl9FruitMachine $curpath & > /dev/null 2>&1