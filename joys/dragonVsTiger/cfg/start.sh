#!/bin/sh
#
# name     : tmuxenï¼ tmux environment made easy
# author   : Biyuan Li bill.allen@163.com
# license  : QKA
# created  : 2017 May 02
# modified : 2017 may 02
#


ulimit -c unlimited
ulimit -n 600000

../bin/shell ./dragonVsTigerServerCfg.xml scene_dvt1 ../bin/libdragonVsTiger & > /dev/null 2>&1

exit 0

