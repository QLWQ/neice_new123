# 拉取执行文件
#git pull

#停joys服操作
cd /app/new/joys
./stop_all.sh
sleep 1

#停platform服操作
cd /app/new/platform/cfg
./stop.sh
sleep 1

#删除bin下login, master, batchLog
#cd /app/new/platform/bin
#rm -f login master batchLog

# 拷贝数据, 系统cp命令已经等同于cp -i 即每项拷贝都需要手动确认， 这里加\ 表示使用系统cp
#cd /home/peanut/data/c_neice
#\cp -rf platform/bin/* /app/new/platform/bin
#\cp -rf platform/csv/* /app/new/platform/csv
#\cp -rf joys/* /app/new/joys

# 修改platform/bin 下文件执行权限
cd /app/new/platform/bin
chmod +x *

# 进入platform/cfg 下cfg下面执行start程序
cd ../cfg
./start.sh

echo "start platform finish!"

# 进入joys 启动游戏
cd /app/new/joys
./start_all.sh

echo "start game finish!"
